#!/bin/bash
for FILE in ./*.pdf; do
  FILENAME=$(echo $FILE| cut -d'.' -f 2)
  pdfcrop "${FILE}" ".${FILENAME}_c.pdf"
done
