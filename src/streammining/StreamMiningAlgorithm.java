package streammining;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class StreamMiningAlgorithm<Item extends Comparable<Item>> {

    private final Map<Item, FrequentItemListEntry<Item>> frequentItemsetList = new HashMap<Item, FrequentItemListEntry<Item>>();
    private final SortedSet<Item> sortedItems = new TreeSet<Item>();
    private final int numThreads;
    private ExecutorService threadPool;
    private final Semaphore addingSemaphore = new Semaphore(0);
    private final Semaphore queryingSemaphore = new Semaphore(0);

    private final boolean pruneRegularily;
    private final int pruningInterval;
    private final double pruneSupportErrorThreshold;
    private int numItemsets = 0;

    public StreamMiningAlgorithm() {
        numThreads = 1;
        threadPool = Executors.newSingleThreadExecutor();
        pruneRegularily = false;
        pruningInterval = -1;
        pruneSupportErrorThreshold = 0.;
    }

    public StreamMiningAlgorithm(int numThreadsArg) {
        numThreads = numThreadsArg;
        threadPool = Executors.newFixedThreadPool(numThreads);
        pruneRegularily = false;
        pruningInterval = -1;
        pruneSupportErrorThreshold = 0.;
    }

    public StreamMiningAlgorithm(int numThreadsArg, int pruningIntervalArg, double supportErrorThreshold) {
        if (numThreadsArg == 1) {
            numThreads = 1;
            threadPool = Executors.newSingleThreadExecutor();
        } else {
            numThreads = numThreadsArg;
            threadPool = Executors.newFixedThreadPool(numThreads);
        }
        pruneRegularily = true;
        pruningInterval = pruningIntervalArg;
        pruneSupportErrorThreshold = supportErrorThreshold;
    }

    public final void addItemset(List<Item> sortedItemset, int windowId) {
        for (int i = 0; i < sortedItemset.size(); ++i)
            addItemsetToFIListEntry(getPostfix(sortedItemset, i), windowId);
        addingSemaphore.acquireUninterruptibly(sortedItemset.size());
        ++numItemsets;
        if (pruneRegularily && numItemsets % pruningInterval == 0)
            prune(pruneSupportErrorThreshold, windowId, true);
    }

    public final List<List<Item>> getFrequentItemsets(double supportThreshold, double supportErrorThreshold, int windowId) {
        return getFrequentItemsets(supportThreshold, supportErrorThreshold, windowId, false);
    }

    public final List<List<Item>> getFrequentItemsets(double supportThreshold, double supportErrorThreshold, int windowId,
                                                      boolean computeSupportFromZero) {
        if (threadPool == null) {
            threadPool = Executors.newFixedThreadPool(numThreads);
        }
        final MaximalFrequentItemsetList<Item> frequentItemssets = new MaximalFrequentItemsetList<Item>();
        for (final Item item : sortedItems)
            computeFrequentItemsetsStartingWithItemParallel(item, supportThreshold, supportErrorThreshold, windowId, frequentItemssets,
                                                            computeSupportFromZero);
        queryingSemaphore.acquireUninterruptibly(sortedItems.size());
        threadPool.shutdownNow();
        frequentItemssets.cleanUp();
        return frequentItemssets;
    }

    public final void prune(double supportErrorThreshold, int windowId) {
        prune(supportErrorThreshold, windowId, false);
    }

    public final void prune(double supportErrorThreshold, int windowId, boolean computeSupportFromZero) {
        final List<Item> itemsToRemove = new ArrayList<Item>();
        for (final FrequentItemListEntry<Item> fiListEntry : frequentItemsetList.values()) {
            final int range = windowId - (computeSupportFromZero ? 0 : fiListEntry.getWindowId());
            if (fiListEntry.getSupport() < supportErrorThreshold * range)
                itemsToRemove.add(fiListEntry.getItem());
        }
        for (final Item item : itemsToRemove)
            removeItem(item);
    }

    public int getNumItemsetsAdded() {
        return numItemsets;
    }

    @Override
    public final String toString() {
        String s = "";
        for (final FrequentItemListEntry<Item> listEntry : frequentItemsetList.values())
            s += listEntry.toString();
        return s;
    }

    public final int getNumNodes() {
        int num = 0;
        for (final FrequentItemListEntry<Item> listEntry : frequentItemsetList.values())
            num += listEntry.getNumNodes();
        return num;
    }

    public final void printToFile(String filename) throws IOException {
        try (FileWriter writer = new FileWriter(filename)) {
            writer.append(toString());
            writer.flush();
            writer.close();
        } catch (final IOException e) {
            throw e;
        }
    }

    private final void addItemsetToFIListEntry(List<Item> sortedItemset, int windowId) {
        final Item item = sortedItemset.get(0);
        if (frequentItemsetList.containsKey(item))
            updateFIListEntryForItem(item, sortedItemset, windowId);
        else {
            sortedItems.add(item);
            frequentItemsetList.put(item, new FrequentItemListEntry<Item>(sortedItemset, windowId));
            addingSemaphore.release();
        }
    }

    private void updateFIListEntryForItem(final Item item, List<Item> sortedItemset, int windowId) {
        final FrequentItemListEntry<Item> listEntry = frequentItemsetList.get(item);
        threadPool.execute(() -> {
            listEntry.addItemset(sortedItemset, windowId);
            addingSemaphore.release();
        });
    }

    private final List<Item> getPostfix(List<Item> itemset, int i) {
        return itemset.subList(i, itemset.size());
    }

    private void computeFrequentItemsetsStartingWithItemParallel(final Item item, double supportThreshold, double supportErrorThreshold, int windowId,
                                                                 final MaximalFrequentItemsetList<Item> frequentItemssetsContainer,
                                                                 boolean computeSupportFromZero) {
        threadPool.execute(() -> {
            final Set<List<Item>> frequentItemsets = computeFrequentItemsetsStartingWithItem(item, supportThreshold, supportErrorThreshold, windowId,
                                                                                             computeSupportFromZero);
            synchronized (frequentItemssetsContainer) {
                frequentItemssetsContainer.addAll(frequentItemsets);
            }
            queryingSemaphore.release();
        });
    }

    private final Set<List<Item>> computeFrequentItemsetsStartingWithItem(final Item item, double supportThreshold, double supportErrorThreshold,
                                                                          int windowId, boolean computeSupportFromZero) {
        final FrequentItemListEntry<Item> fiListEntry = frequentItemsetList.get(item);
        final int range = windowId - (computeSupportFromZero ? 0 : fiListEntry.getWindowId());
        final int minSupport = (int) ((supportThreshold - supportErrorThreshold) * range);
        return fiListEntry.getMaximalFrequentItemsets(Integer.max(1, minSupport));
    }

    private final void removeItem(final Item item) {
        frequentItemsetList.remove(item);
        sortedItems.remove(item);
        for (final FrequentItemListEntry<Item> otherFiListEntry : frequentItemsetList.values())
            otherFiListEntry.removeItem(item);
    }

}
