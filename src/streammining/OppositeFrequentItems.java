package streammining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

class OppositeFrequentItems<Item extends Comparable<Item>> {

    private final Map<Item, ListNode<Item>> entries = new HashMap<Item, ListNode<Item>>();
    private final SortedSet<Item> sortedItems = new TreeSet<Item>();

    public final void addEntry(Item itemArg, int windowId, ListNode<Item> connectedNodeArg) {
        if (entries.containsKey(itemArg))
            updateEntry(itemArg, connectedNodeArg);
        else {
            entries.put(itemArg, new ListNode<Item>(itemArg, windowId, connectedNodeArg));
            sortedItems.add(itemArg);
        }
    }

    public final boolean contains(Item item) {
        return entries.containsKey(item);
    }

    public final int getSupport(Item ofItem) {
        return entries.get(ofItem).getSupport();
    }

    public final List<ListNode<Item>> getMaximalSetOfFrequentItems(int minSupport) {
        final List<ListNode<Item>> itemset = new ArrayList<ListNode<Item>>();
        for (final Item item : sortedItems) {
            final ListNode<Item> itemEntry = entries.get(item);
            if (itemEntry.getSupport() >= minSupport)
                itemset.add(itemEntry);
        }
        return itemset;
    }

    public final ListNode<Item> deleteItem(Item itemArg) {
        sortedItems.remove(itemArg);
        return entries.remove(itemArg);
    }

    private final void updateEntry(Item itemArg, ListNode<Item> connectedNodeArg) {
        final ListNode<Item> entry = entries.get(itemArg);
        if (connectedNodeArg != null) {
            connectedNodeArg.setNextNode(entry.getNextNode());
            entry.setNextNode(connectedNodeArg);
        }
        entry.incrementSupport();
    }

}
