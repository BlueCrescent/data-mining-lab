package streammining.preprocessor;

import java.util.Collections;
import java.util.List;

public class SortingPreprocessor<Item extends Comparable<Item>> implements StreamMiningPreprocessor<Item, Item> {

    @Override
    public List<Item> process(List<Item> itemset) {
        Collections.sort(itemset);
        return itemset;
    }

}
