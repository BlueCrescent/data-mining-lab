package streammining.preprocessor;

import java.util.List;

public interface StreamMiningPreprocessor<ItemIn, ItemOut> {

    public List<ItemOut> process(List<ItemIn> itemset);

}
