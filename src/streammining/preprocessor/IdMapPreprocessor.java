package streammining.preprocessor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class IdMapPreprocessor<Item> implements List<Item>, StreamMiningPreprocessor<Item, Integer> {

    private final StreamMiningPreprocessor<Item, Item> other;
    private final Map<Item, Integer> itemToId = new HashMap<Item, Integer>();
    private final List<Item> items = new ArrayList<Item>();

    public IdMapPreprocessor() {
        other = null;
    }

    public IdMapPreprocessor(StreamMiningPreprocessor<Item, Item> preprocessorArg) {
        other = preprocessorArg;
    }

    @Override
    public List<Integer> process(List<Item> itemset) {
        if (other != null)
            itemset = other.process(itemset);
        return transformItemset(itemset);
    }

    @Override
    public boolean add(Item arg0) {
        if (itemToId.containsKey(arg0))
            return false;
        createNewId(arg0);
        return true;
    }

    @Override
    public void add(int arg0, Item arg1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends Item> arg0) {
        boolean modified = false;
        for (final Item item : arg0)
            if (add(item))
                modified = true;
        return modified;
    }

    @Override
    public boolean addAll(int arg0, Collection<? extends Item> arg1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        itemToId.clear();
        items.clear();
    }

    @Override
    public boolean contains(Object arg0) {
        return itemToId.containsKey(arg0);
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        boolean containsAll = true;
        for (final Object o : arg0)
            if (!contains(o))
                containsAll = false;
        return containsAll;
    }

    @Override
    public Item get(int arg0) {
        return items.get(arg0);
    }

    @Override
    public int indexOf(Object arg0) {
        final Integer id = itemToId.get(arg0);
        return id != null ? id : -1;
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public Iterator<Item> iterator() {
        return items.iterator();
    }

    @Override
    public int lastIndexOf(Object arg0) {
        return indexOf(arg0);
    }

    @Override
    public ListIterator<Item> listIterator() {
        return items.listIterator();
    }

    @Override
    public ListIterator<Item> listIterator(int arg0) {
        return items.listIterator(arg0);
    }

    @Override
    public boolean remove(Object arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item remove(int arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item set(int arg0, Item arg1) {
        if (itemToId.containsKey(arg1))
            throw new IllegalArgumentException("Item already set!");
        final Item prevItem = items.set(arg0, arg1);
        itemToId.remove(prevItem);
        itemToId.put(arg1, arg0);
        return prevItem;
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public List<Item> subList(int arg0, int arg1) {
        return items.subList(arg0, arg1);
    }

    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        return items.toArray(arg0);
    }

    private List<Integer> transformItemset(List<Item> itemset) {
        final List<Integer> newItemset = new ArrayList<Integer>();
        for (final Item item : itemset)
            newItemset.add(transformItemToId(item));
        return newItemset;
    }

    private Integer transformItemToId(Item item) {
        Integer id = itemToId.get(item);
        if (id == null)
            id = createNewId(item);
        return id;
    }

    private Integer createNewId(Item item) {
        Integer id;
        id = items.size();
        itemToId.put(item, id);
        items.add(item);
        return id;
    }

}
