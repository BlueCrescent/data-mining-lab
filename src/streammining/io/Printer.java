package streammining.io;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Printer {

    public static final void writeStringItemsetsToFile(String filename, String separator, List<List<String>> itemsets) throws IOException {
        writeStringItemsetsToFile(filename, separator, itemsets, false);
    }

    public static final void writeStringItemsetsToFile(String filename, String separator, List<List<String>> itemsets,
                                                       boolean append) throws IOException {
        try (FileWriter writer = new FileWriter(filename, append)) {
            for (final List<String> itemset : itemsets)
                writer.append(String.join(separator, itemset) + "\n");
            writer.flush();
            writer.close();
        } catch (final IOException e) {
            throw e;
        }
    }

    public static final void writeIntegerItemsetsToFile(String filename, String separator, List<List<Integer>> itemsets) throws IOException {
        writeIntegerItemsetsToFile(filename, separator, itemsets, false);
    }

    public static final void writeIntegerItemsetsToFile(String filename, String separator, List<List<Integer>> itemsets,
                                                        boolean append) throws IOException {
        try (FileWriter writer = new FileWriter(filename, append)) {
            for (final List<Integer> itemset : itemsets) {
                final List<String> strItemset = new ArrayList<String>(itemset.size());
                for (final Integer item : itemset)
                    strItemset.add(String.valueOf(item));
                writer.append(String.join(separator, strItemset) + "\n");
            }
            writer.flush();
            writer.close();
        } catch (final IOException e) {
            throw e;
        }
    }

}
