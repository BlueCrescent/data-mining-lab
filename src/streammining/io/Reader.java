package streammining.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reader {

    public static final List<List<String>> readStringItemsets(String filename, String separator) throws IOException {
        final List<List<String>> itemsets = new ArrayList<List<String>>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                if (line.endsWith(separator))
                    line = line.substring(0, line.length() - separator.length());
                itemsets.add(Arrays.asList(line.split(separator)));
            }
        } catch (final IOException e) {
            throw e;
        }
        return itemsets;
    }

    public static final List<List<Integer>> readIntegerItemsets(String filename, String separator) throws IOException {
        final List<List<Integer>> itemsets = new ArrayList<List<Integer>>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                try {
                    if (line.endsWith(separator))
                        line = line.substring(0, line.length() - separator.length());
                    final List<Integer> itemset = new ArrayList<Integer>();
                    for (final String item : line.split(separator))
                        itemset.add(Integer.parseInt(item));
                    itemsets.add(itemset);
                } catch (final NumberFormatException e) {
                }
            }
        } catch (final IOException e) {
            throw e;
        }
        return itemsets;
    }

}
