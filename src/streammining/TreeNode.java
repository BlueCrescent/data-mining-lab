package streammining;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

class TreeNode<Item extends Comparable<Item>> extends ListNode<Item> {

    protected TreeNode<Item> parent;
    protected final Map<Item, TreeNode<Item>> children = new HashMap<Item, TreeNode<Item>>();

    public TreeNode(Item itemArg, int windowIdArg) {
        super(itemArg, windowIdArg);
        parent = null;
    }

    public TreeNode(Item itemArg, int windowIdArg, TreeNode<Item> parentArg) {
        super(itemArg, windowIdArg);
        parent = parentArg;
    }

    public final boolean hasChildren() {
        return !children.isEmpty();
    }

    public final boolean hasChildForItem(Item itemArg) {
        return children.containsKey(itemArg);
    }

    public final Collection<TreeNode<Item>> getChildren() {
        return children.values();
    }

    public final TreeNode<Item> getChildForItem(Item itemArg) {
        return children.get(itemArg);
    }

    public TreeNode<Item> getParent() {
        return parent;
    }

    public final TreeNode<Item> addChild(Item itemArg, int windowId) {
        if (hasChildForItem(itemArg))
            return updateChild(itemArg);
        return buildNewChild(itemArg, windowId);
    }

    public final void remove() {
        parent.children.remove(item);
        for (final Map.Entry<Item, TreeNode<Item>> child : children.entrySet())
            parent.mergeChild(child);
    }

    public final String toString(int indentLevel) {
        String s = new String(new char[indentLevel]).replace("\0", "-") + String.valueOf(item) + "\n";
        for (final TreeNode<Item> child : children.values())
            s += child.toString(indentLevel + 1);
        return s;
    }

    public final int getNumNodes() {
        int num = 1;
        for (final TreeNode<Item> child : children.values())
            num += child.getNumNodes();
        return num;
    }

    protected final void merge(TreeNode<Item> otherNode) {
        assert (this.item == otherNode.item);
        support += otherNode.support;
        if (windowId > otherNode.windowId)
            windowId = otherNode.windowId;
        for (final Map.Entry<Item, TreeNode<Item>> child : otherNode.children.entrySet())
            mergeChild(child);
    }

    private final void mergeChild(Map.Entry<Item, TreeNode<Item>> child) {
        if (children.containsKey(child.getKey()))
            children.get(child.getKey()).merge(child.getValue());
        else {
            children.put(child.getKey(), child.getValue());
            child.getValue().parent = this;
        }
    }

    private final TreeNode<Item> updateChild(Item itemArg) {
        final TreeNode<Item> child = children.get(itemArg);
        child.incrementSupport();
        return child;
    }

    private final TreeNode<Item> buildNewChild(Item itemArg, int windowId) {
        final TreeNode<Item> newNode = new TreeNode<Item>(itemArg, windowId, this);
        children.put(itemArg, newNode);
        return newNode;
    }

}
