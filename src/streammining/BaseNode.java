package streammining;

class BaseNode<Item extends Comparable<Item>> {

    protected final Item item;
    protected int support;
    protected int windowId;

    public BaseNode(Item itemArg, int windowIdArg) {
        item = itemArg;
        support = 1;
        windowId = windowIdArg;
    }

    public final Item getItem() {
        return item;
    }

    public final int getSupport() {
        return support;
    }

    public final int getWindowId() {
        return windowId;
    }

    public final void incrementSupport() {
        support++;
    }
}
