package streammining;

import java.util.Iterator;
import java.util.NoSuchElementException;

class ListNode<Item extends Comparable<Item>> extends BaseNode<Item> implements Iterable<ListNode<Item>> {

    protected ListNode<Item> nextNode;

    public ListNode(Item itemArg, int windowIdArg) {
        super(itemArg, windowIdArg);
        nextNode = null;
    }

    public ListNode(Item itemArg, int windowIdArg, ListNode<Item> nextNodeArg) {
        super(itemArg, windowIdArg);
        nextNode = nextNodeArg;
    }

    @Override
    public final Iterator<ListNode<Item>> iterator() {
        return new Iterator<ListNode<Item>>() {

            ListNode<Item> node = ListNode.this;

            @Override
            public boolean hasNext() {
                return node.hasNext();
            }

            @Override
            public ListNode<Item> next() {
                node = node.getNextNode();
                if (node == null)
                    throw new NoSuchElementException();
                return node;
            }

        };
    }

    public final boolean hasNext() {
        return nextNode != null;
    }

    public final ListNode<Item> getNextNode() {
        return nextNode;
    }

    public final void setNextNode(ListNode<Item> nextNodeArg) {
        nextNode = nextNodeArg;
    }

}
