package streammining;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

class FrequentItemListEntry<Item extends Comparable<Item>> {

    private final OppositeFrequentItems<Item> ofiList = new OppositeFrequentItems<Item>();
    private final TreeNode<Item> sfiTree;

    public FrequentItemListEntry(List<Item> sortedItemset, int windowId) {
        sfiTree = new TreeNode<Item>(sortedItemset.get(0), windowId);
        updateSFITreeAndOFIList(sortedItemset.subList(1, sortedItemset.size()), windowId);
    }

    public final Item getItem() {
        return sfiTree.getItem();
    }

    public final int getSupport() {
        return sfiTree.getSupport();
    }

    public final int getWindowId() {
        return sfiTree.getWindowId();
    }

    public final Set<List<Item>> getMaximalFrequentItemsets(int minSupport) {
        final Set<List<Item>> results = new HashSet<List<Item>>();
        if (getSupport() < minSupport)
            return results;
        getFrequentItemsetsStartingWithMaximalCandidateSet(minSupport, results);
        return results;
    }

    public final void addItemset(List<Item> sortedItemset, int windowId) {
        assert (sortedItemset.get(0) == getItem());
        sfiTree.incrementSupport();
        updateSFITreeAndOFIList(sortedItemset.subList(1, sortedItemset.size()), windowId);
    }

    public final void removeItem(Item itemArg) {
        assert itemArg != getItem() : "Cannot remove main item of FI list entry.";
        final ListNode<Item> itemNodeList = ofiList.deleteItem(itemArg);
        if (itemNodeList != null)
            for (final ListNode<Item> node : itemNodeList)
                ((TreeNode<Item>) node).remove();
    }

    @Override
    public final String toString() {
        return sfiTree.toString(0);
    }

    public final int getNumNodes() {
        return sfiTree.getNumNodes();
    }

    private final void getFrequentItemsetsStartingWithMaximalCandidateSet(int minSupport, final Set<List<Item>> results) {
        final List<ListNode<Item>> candidateSet = ofiList.getMaximalSetOfFrequentItems(minSupport);
        if (candidateSet.isEmpty())
            addItemsetToResults(toItemList(null), results);
        else
            getMaximalFrequentItemsets(minSupport, candidateSet, getItem(), results);
    }

    private final void getMaximalFrequentItemsets(int minSupport, List<ListNode<Item>> candidateSet, Item lastRemoved, Set<List<Item>> results) {
        if (isSupported(minSupport, candidateSet))
            addItemsetToResults(toItemList(candidateSet), results);
        else
            getMaximalFrequentItemsetsInSubsets(minSupport, candidateSet, lastRemoved, results);
    }

    private final void getMaximalFrequentItemsetsInSubsets(int minSupport, List<ListNode<Item>> candidateSet, Item lastRemoved,
                                                           Set<List<Item>> results) {
        if (candidateSet.size() <= 1)
            return;
        for (int i = 0; i < candidateSet.size(); ++i)
            getMaximalFrequentItemInMinusOneSubset(minSupport, candidateSet, lastRemoved, i, results);
    }

    private final void getMaximalFrequentItemInMinusOneSubset(int minSupport, List<ListNode<Item>> candidateSet, Item lastRemoved,
                                                              int indexToBeRemoved, Set<List<Item>> results) {
        final Item itemToRemove = candidateSet.get(indexToBeRemoved).getItem();
        if (lastRemoved.compareTo(itemToRemove) < 0)
            getMaximalFrequentItemsets(minSupport, minusOneSubset(candidateSet, indexToBeRemoved), itemToRemove, results);
    }

    private final List<ListNode<Item>> minusOneSubset(List<ListNode<Item>> itemset, int indexToRemove) {
        final List<ListNode<Item>> subset = new ArrayList<ListNode<Item>>(itemset.subList(0, indexToRemove));
        subset.addAll(itemset.subList(indexToRemove + 1, itemset.size()));
        return subset;
    }

    private final List<Item> toItemList(List<ListNode<Item>> candidateSet) {
        final List<Item> resultSet = new ArrayList<Item>();
        resultSet.add(getItem());
        if (candidateSet != null)
            for (final ListNode<Item> node : candidateSet)
                resultSet.add(node.getItem());
        return resultSet;
    }

    private final boolean isSupported(int minSupport, List<ListNode<Item>> itemset) {
        int support = 0;
        // for (final ListNode<Item> startNode : itemset.get(0))
        // support += computeSupportInTree((TreeNode<Item>) startNode, itemset);
        for (final ListNode<Item> startNode : itemset.get(itemset.size() - 1))
            support += computeSupportInTreeBottomUp((TreeNode<Item>) startNode, itemset);
        return minSupport <= support;
    }

    private final int computeSupportInTree(TreeNode<Item> root, List<ListNode<Item>> itemset) {
        return computeSupportInTree(root, itemset, 0);
    }

    private final int computeSupportInTree(TreeNode<Item> currentNode, List<ListNode<Item>> itemset, int itemIndex) {
        if (itemIndex == itemset.size())
            return getSupport();
        if (nodeIsSupportingCurrentItem(currentNode, itemset, itemIndex))
            return Math.min(currentNode.getSupport(), computeSupportInTree(currentNode, itemset, itemIndex + 1));
        if (!currentNode.hasChildren())
            return 0;
        return aggregateSubtreeSupport(currentNode, itemset, itemIndex);
    }

    private final int computeSupportInTreeBottomUp(TreeNode<Item> fromNode, List<ListNode<Item>> itemset) {
        TreeNode<Item> currentNode = fromNode;
        final ListIterator<ListNode<Item>> iter = itemset.listIterator(itemset.size());
        while (iter.hasPrevious()) {
            currentNode = getPositionForItemInPathToRoot(iter.previous().getItem(), currentNode);
            if (currentNode == null)
                return 0;
        }
        return fromNode.getSupport();
    }

    private TreeNode<Item> getPositionForItemInPathToRoot(Item item, TreeNode<Item> currentPosition) {
        while (!currentPosition.getItem().equals(item)) {
            if (currentPosition.getItem().equals(getItem()))
                return null;
            currentPosition = currentPosition.getParent();
        }
        return currentPosition;
    }

    private final boolean nodeIsSupportingCurrentItem(TreeNode<Item> node, List<ListNode<Item>> itemset, int itemIndex) {
        return node.getItem().equals(itemset.get(itemIndex).getItem());
    }

    private final int aggregateSubtreeSupport(TreeNode<Item> root, List<ListNode<Item>> itemset, int itemsetIndex) {
        int aggregatedSupport = 0;
        for (final TreeNode<Item> childNode : root.getChildren())
            aggregatedSupport += computeSupportInTree(childNode, itemset, itemsetIndex);
        return Math.min(root.getSupport(), aggregatedSupport);
    }

    private final void updateSFITreeAndOFIList(List<Item> sortedSubItemset, int windowId) {
        TreeNode<Item> lastNode = sfiTree;
        for (final Item item : sortedSubItemset) {
            final boolean isOldNode = lastNode.hasChildForItem(item);
            lastNode = lastNode.addChild(item, windowId);
            ofiList.addEntry(item, windowId, isOldNode ? null : lastNode);
        }
    }

    private final void addItemsetToResults(List<Item> sortedItemset, Set<List<Item>> results) {
        for (final Iterator<List<Item>> iter = results.iterator(); iter.hasNext();) {
            final int compareVal = compareSets(sortedItemset, iter.next());
            if (compareVal < 0)
                return;
            if (compareVal > 0)
                iter.remove();
        }
        results.add(sortedItemset);
    }

    // Returns -1 if itemset1 is subset of or equal to itemset2, 1 if itemset2 is
    // true subset of itemset1 and 0 otherwise.
    private final static <Item extends Comparable<Item>> int compareSets(List<Item> itemset1, List<Item> itemset2) {
        int i1 = 0, i2 = 0;
        boolean hasItemsetOneOnlyItems = false, hasItemsetTwoOnlyItems = false;
        while (i1 < itemset1.size() && i2 < itemset2.size()) {
            final int compareVal = itemset1.get(i1).compareTo(itemset2.get(i2));
            if (compareVal == 0) {
                ++i1;
                ++i2;
            } else if (compareVal < 0) {
                hasItemsetOneOnlyItems = true;
                ++i1;
            } else {
                hasItemsetTwoOnlyItems = true;
                ++i2;
            }
        }
        if (i1 < itemset1.size())
            hasItemsetOneOnlyItems = true;
        else if (i2 < itemset2.size())
            hasItemsetTwoOnlyItems = true;
        if (hasItemsetOneOnlyItems && hasItemsetTwoOnlyItems)
            return 0;
        if (!hasItemsetOneOnlyItems)
            return -1;
        return 1;
    }

}
