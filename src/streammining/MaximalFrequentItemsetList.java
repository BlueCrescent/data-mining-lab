package streammining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class MaximalFrequentItemsetList<Item> extends ArrayList<List<Item>> {

    /**
     * Generated serial version uid.
     */
    private static final long serialVersionUID = 239212716787636878L;

    private Map<Item, Set<Integer>> itemToIndexMap = new HashMap<Item, Set<Integer>>();

    /**
     * Adds a given itemset if no superset is already contained in the list.
     *
     * @param sortedItemset
     *            the itemset to be considered for adding
     * @return true if this collection changed as a result of the call
     */
    @Override
    public boolean add(List<Item> itemset) {
        if (isSubsetOfExistingSet(itemset))
            return false;
        for (final Item item : itemset)
            addItemIndexMapping(item, size());
        return super.add(itemset);
    }

    /**
     * Adds given itemsets if for the given set no superset is already contained in
     * the list.
     *
     * @param sortedItemsets
     *            the itemsets to be considered for adding
     * @return true if this collection changed as a result of the call
     */
    @Override
    public boolean addAll(Collection<? extends List<Item>> sortedItemsets) {
        boolean wasModified = false;
        for (final List<Item> itemset : sortedItemsets)
            if (add(itemset))
                wasModified = true;
        return wasModified;
    }

    /**
     * Removes structure for checking if supersets of given sets are already
     * contained. DO NOT ADD NEW SETS AFTER CALLING THIS.
     */
    public void cleanUp() {
        itemToIndexMap = null;
    }

    @Override
    public void clear() {
        super.clear();
        itemToIndexMap.clear();
    }

    private boolean isSubsetOfExistingSet(List<Item> itemset) {
        if (isEmpty() || !itemToIndexMap.containsKey(itemset.get(0)))
            return false;
        final Set<Integer> supersetIndices = new HashSet<Integer>(itemToIndexMap.get(itemset.get(0)));
        for (final Item item : itemset.subList(1, itemset.size())) {
            final Set<Integer> indices = itemToIndexMap.get(item);
            if (indices == null)
                return false;
            supersetIndices.retainAll(indices);
        }
        return supersetIndices.size() > 0;
    }

    private void addItemIndexMapping(Item item, int index) {
        if (!itemToIndexMap.containsKey(item))
            itemToIndexMap.put(item, new HashSet<Integer>());
        itemToIndexMap.get(item).add(index);
    }
}
