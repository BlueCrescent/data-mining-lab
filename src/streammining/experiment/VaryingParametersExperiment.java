package streammining.experiment;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VaryingParametersExperiment {

    public static void main(String[] args) {
        checkNumberOfArgs(args);

        final String inputFilename = args[0];
        final String outputFilename = args[1];
        final String outputFilenameStats = args[1] + ".stats";
        final double pruningSupportErrorThreshold = Double.parseDouble(args[3]);
        final List<Double> supportThresholds = new ArrayList<Double>();
        final List<Double> supportErrorThresholds = new ArrayList<Double>();
        for (int i = 4; i < args.length - 1; i += 2) {
            supportThresholds.add(Double.parseDouble(args[i]));
            supportErrorThresholds.add(Double.parseDouble(args[i + 1]));
        }
        final String separator = args[2];
        final int numThreads = 1;

        final List<List<Integer>> itemsets = ExperimentHelpers.readInput(inputFilename, separator);

        final ExperimentStatistics algoStats = ExperimentHelpers.runAlgorithm(supportThresholds, supportErrorThresholds, pruningSupportErrorThreshold,
                                                                              itemsets, numThreads, true);

        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename }), outputFilenameStats, separator, algoStats, false);
    }

    private static void checkNumberOfArgs(String[] args) {
        if (args.length < 4) {
            String execName;
            execName = new File(VaryingParametersExperiment.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
            System.out.println("Start with arguments: \"java " + execName
                               + " inputFilename outputFilename separator pruningSupportErrorThreshold minimumFrequqencyThresholdFactor1 errorThresholdFactor1 minimumFrequqencyThresholdFactor2 errorThresholdFactor2 ...\"");
            System.exit(1);
        }
    }
}
