package streammining.experiment;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import streammining.StreamMiningAlgorithm;
import streammining.io.Reader;

public class ExperimentHelpers {

    public static void getCurrentMemoryUsage(final Map<String, Long> used, final Map<String, Long> reserved) {
        final List<MemoryPoolMXBean> pools = ManagementFactory.getMemoryPoolMXBeans();
        for (final MemoryPoolMXBean pool : pools) {
            final MemoryUsage peak = pool.getPeakUsage();
            used.put(pool.getName(), peak.getUsed());
            reserved.put(pool.getName(), peak.getCommitted());
        }
    }

    public static ExperimentStatistics runAlgorithm(final double supportThreshold, final double supportErrorThreshold,
                                                    final List<List<Integer>> itemsets, boolean computeSupportFromZero) {
        return runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 1, computeSupportFromZero);
    }

    public static ExperimentStatistics runAlgorithm(final double supportThreshold, final double supportErrorThreshold,
                                                    final List<List<Integer>> itemsets, int numThreads, boolean computeSupportFromZero) {
        return runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, numThreads, 1, computeSupportFromZero);
    }

    public static ExperimentStatistics runAlgorithm(final double supportThreshold, final double supportErrorThreshold,
                                                    final List<List<Integer>> itemsets, int numThreads, int numQueries,
                                                    boolean computeSupportFromZero) {
        final List<Double> supportThresholds = new ArrayList<Double>(numQueries);
        final List<Double> supportErrorThresholds = new ArrayList<Double>(numQueries);
        for (int i = 0; i < numQueries; ++i) {
            supportThresholds.add(supportThreshold);
            supportErrorThresholds.add(supportErrorThreshold);
        }
        return runAlgorithm(supportThresholds, supportErrorThresholds, supportErrorThreshold, itemsets, numThreads, computeSupportFromZero);
    }

    public static ExperimentStatistics runAlgorithm(final List<Double> supportThresholds, final List<Double> supportErrorThresholds,
                                                    double pruningSupportErrorThreshold, final List<List<Integer>> itemsets, int numThreads,
                                                    boolean computeSupportFromZero) {
        return runAlgorithm(supportThresholds, supportErrorThresholds, pruningSupportErrorThreshold, itemsets, numThreads, computeSupportFromZero,
                            -1);
    }

    public static ExperimentStatistics runAlgorithm(final double supportThreshold, final double supportErrorThreshold,
                                                    final List<List<Integer>> itemsets, boolean computeSupportFromZero, int pruningInterval) {
        final List<Double> supportThresholds = new ArrayList<Double>(1);
        final List<Double> supportErrorThresholds = new ArrayList<Double>(1);
        supportThresholds.add(supportThreshold);
        supportErrorThresholds.add(supportErrorThreshold);
        return runAlgorithm(supportThresholds, supportErrorThresholds, supportErrorThreshold, itemsets, 1, computeSupportFromZero, pruningInterval);
    }

    public static ExperimentStatistics runAlgorithm(final List<Double> supportThresholds, final List<Double> supportErrorThresholds,
                                                    double pruningSupportErrorThreshold, final List<List<Integer>> itemsets, int numThreads,
                                                    boolean computeSupportFromZero, int pruningInterval) {
        final ExperimentStatistics stats = new ExperimentStatistics();

        final long startTimeSort = System.currentTimeMillis();
        for (final List<Integer> itemset : itemsets)
            Collections.sort(itemset);
        final long stopTimeSort = System.currentTimeMillis();
        stats.elapsedTimeSort = stopTimeSort - startTimeSort;
        getCurrentMemoryUsage(stats.memoryAfterSort_Used, stats.memoryAfterSort_Reserved);

        final long startTimeAdd = System.currentTimeMillis();
        final StreamMiningAlgorithm<Integer> algo = pruningInterval > 0 ? new StreamMiningAlgorithm<Integer>(numThreads, pruningInterval,
                                                                                                             pruningSupportErrorThreshold)
                                                                        : new StreamMiningAlgorithm<Integer>(numThreads);
        for (int windowId = 0; windowId < itemsets.size(); ++windowId)
            algo.addItemset(itemsets.get(windowId), windowId);
        final long stopTimeAdd = System.currentTimeMillis();
        stats.elapsedTimeAdd = stopTimeAdd - startTimeAdd;
        getCurrentMemoryUsage(stats.memoryAfterAdd_Used, stats.memoryAfterAdd_Reserved);
        stats.numNodesBeforePrune = algo.getNumNodes();

        final long startTimePrune = System.currentTimeMillis();
        algo.prune(pruningSupportErrorThreshold, itemsets.size() - 1, computeSupportFromZero);
        final long stopTimePrune = System.currentTimeMillis();
        stats.elapsedTimePrune = stopTimePrune - startTimePrune;
        getCurrentMemoryUsage(stats.memoryAfterPrune_Used, stats.memoryAfterPrune_Reserved);
        stats.numNodesAfterPrune = algo.getNumNodes();

        // try (FileWriter writer = new FileWriter("prefix_tree.txt")) {
        // writer.append(algo.toString());
        // writer.flush();
        // writer.close();
        // } catch (final IOException e) {
        // System.err.println("Error while writing prefix tree: " + e.getMessage());
        // }

        for (int i = 0; i < supportThresholds.size(); ++i) {
            final long startTimeGet = System.currentTimeMillis();
            stats.storeResults(algo.getFrequentItemsets(supportThresholds.get(i), supportErrorThresholds.get(i), itemsets.size() - 1,
                                                        computeSupportFromZero));
            final long stopTimeGet = System.currentTimeMillis();
            stats.addQueryTime(stopTimeGet - startTimeGet);
            stats.addCurrentMemoryToAfterQueryStats();
        }

        return stats;
    }

    public static void printResults(final List<String> outputFilenames, final String statsFilename, final String separator,
                                    final ExperimentStatistics algoStats, boolean appendStats) {
        try {
            algoStats.printStats(statsFilename, appendStats);
            algoStats.printResutls(outputFilenames, separator);
        } catch (final IOException e) {
            System.err.println("Error while printing to files correspong to \"" + statsFilename + "\":");
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static List<List<Integer>> readInput(final String inputFilename, final String separator) {
        try {
            return Reader.readIntegerItemsets(inputFilename, separator);
        } catch (final IOException e) {
            System.err.println("Error while reading file \"" + inputFilename + "\":");
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

}
