package streammining.experiment;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import streammining.io.Printer;

public class ExperimentStatistics {

    public int numNodesBeforePrune;
    public int numNodesAfterPrune;

    public long elapsedTimeSort;
    public long elapsedTimeAdd;
    public long elapsedTimePrune;
    public final List<Long> elapsedTimeGet = new ArrayList<Long>();

    public final Map<String, Long> memoryAfterSort_Used = new TreeMap<String, Long>();
    public final Map<String, Long> memoryAfterSort_Reserved = new TreeMap<String, Long>();
    public final Map<String, Long> memoryAfterAdd_Used = new TreeMap<String, Long>();
    public final Map<String, Long> memoryAfterAdd_Reserved = new TreeMap<String, Long>();
    public final Map<String, Long> memoryAfterPrune_Used = new TreeMap<String, Long>();
    public final Map<String, Long> memoryAfterPrune_Reserved = new TreeMap<String, Long>();
    public final List<Map<String, Long>> memoryAfterGet_Used = new ArrayList<Map<String, Long>>();
    public final List<Map<String, Long>> memoryAfterGet_Reserved = new ArrayList<Map<String, Long>>();

    public final List<List<List<Integer>>> frequentItemsetResults;

    public ExperimentStatistics() {
        this(true);
    }

    public ExperimentStatistics(boolean storeQueryResults) {
        if (storeQueryResults)
            frequentItemsetResults = new ArrayList<List<List<Integer>>>();
        else
            frequentItemsetResults = null;
    }

    public void addCurrentMemoryToAfterSortStats() {
        ExperimentHelpers.getCurrentMemoryUsage(memoryAfterSort_Used, memoryAfterSort_Reserved);
    }

    public void addCurrentMemoryToAfterQueryStats() {
        final Map<String, Long> used = new TreeMap<String, Long>();
        final Map<String, Long> reserved = new TreeMap<String, Long>();
        ExperimentHelpers.getCurrentMemoryUsage(used, reserved);
        memoryAfterGet_Used.add(used);
        memoryAfterGet_Reserved.add(reserved);
    }

    public void addQueryTime(long time) {
        elapsedTimeGet.add(time);
    }

    public void storeResults(List<List<Integer>> frequentItemsetResultsArg) {
        if (frequentItemsetResults != null)
            frequentItemsetResults.add(frequentItemsetResultsArg);
    }

    public void printStats(String filename, boolean append) {
        try (FileWriter writer = new FileWriter(filename, append)) {
            writer.append("num nodes before pruning:  " + String.valueOf(numNodesBeforePrune) + "\n");
            writer.append("num nodes after pruning:  " + String.valueOf(numNodesAfterPrune) + "\n");
            printRuntimes(writer);
            printMemoryAfterSort(writer);
            printMemoryAfterAdd(writer);
            printMemoryAfterPrune(writer);
            printMemoryAfterGet(writer);
            writer.flush();
            writer.close();
        } catch (final IOException e) {
            System.err.println("Error while writing stats: " + e.getMessage());
        }
    }

    public void printResutls(final List<String> filenames, String separator) throws IOException {
        if (frequentItemsetResults != null) {
            assert (filenames.size() == frequentItemsetResults.size());
            for (int i = 0; i < filenames.size(); ++i)
                Printer.writeIntegerItemsetsToFile(filenames.get(i), separator, frequentItemsetResults.get(i));
        }
    }

    private void printRuntimes(FileWriter writer) throws IOException {
        writer.append("sort time (ms):  " + String.valueOf(elapsedTimeSort) + "\n");
        writer.append("add time (ms):   " + String.valueOf(elapsedTimeAdd) + "\n");
        writer.append("prune time (ms): " + String.valueOf(elapsedTimePrune) + "\n");
        for (int i = 0; i < elapsedTimeGet.size(); ++i)
            writer.append("get (" + String.valueOf(i + 1) + ") time (ms):   " + String.valueOf(elapsedTimeGet.get(i)) + "\n");
        System.out.println("sort time (ms):  " + String.valueOf(elapsedTimeSort) + "\n");
        System.out.println("add time (ms):   " + String.valueOf(elapsedTimeAdd) + "\n");
        System.out.println("prune time (ms): " + String.valueOf(elapsedTimePrune) + "\n");
        for (int i = 0; i < elapsedTimeGet.size(); ++i)
            System.out.println("get (" + String.valueOf(i + 1) + ") time (ms):   " + String.valueOf(elapsedTimeGet.get(i)) + "\n");
    }

    private void printMemoryAfterSort(FileWriter writer) throws IOException {
        writer.append("memory use after sorting itemsets:\n");
        printMemory(writer, memoryAfterSort_Used, memoryAfterSort_Reserved);
    }

    private void printMemoryAfterAdd(FileWriter writer) throws IOException {
        writer.append("memory use after adding itemsets:\n");
        printMemory(writer, memoryAfterAdd_Used, memoryAfterAdd_Reserved);
    }

    private void printMemoryAfterPrune(FileWriter writer) throws IOException {
        writer.append("memory use after pruning:\n");
        printMemory(writer, memoryAfterPrune_Used, memoryAfterPrune_Reserved);
    }

    private void printMemoryAfterGet(FileWriter writer) throws IOException {
        for (int i = 0; i < memoryAfterGet_Used.size(); ++i) {
            writer.append("memory use after parsing frequent itemsets (" + String.valueOf(i + 1) + "):\n");
            printMemory(writer, memoryAfterGet_Used.get(i), memoryAfterGet_Reserved.get(i));
        }
    }

    private void printMemory(final FileWriter writer, final Map<String, Long> used, final Map<String, Long> reserved) throws IOException {
        for (final String poolName : used.keySet()) {
            writer.append("peak " + poolName + " memory used: " + String.valueOf(used.get(poolName)) + "\n");
            writer.append("peak " + poolName + " memory reserved: " + String.valueOf(reserved.get(poolName)) + "\n");
        }
    }
}
