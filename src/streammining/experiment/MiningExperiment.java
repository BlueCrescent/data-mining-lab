package streammining.experiment;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class MiningExperiment {

    public static void main(String[] args) {
        checkNumberOfArgs(args);

        final String inputFilename = args[0];
        final String outputFilename = args[1];
        final String outputFilenameStats = args[1] + ".stats";
        final double supportThreshold = Double.parseDouble(args[2]);
        final double supportErrorThreshold = Double.parseDouble(args[3]);
        final String separator = args.length < 5 ? "," : args[4];
        final int pruningInterval = args.length < 6 ? -1 : Integer.parseInt(args[5]);

        final List<List<Integer>> itemsets = ExperimentHelpers.readInput(inputFilename, separator);

        final ExperimentStatistics algoStats = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, true,
                                                                              pruningInterval);

        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename }), outputFilenameStats, separator, algoStats, false);
    }

    private static void checkNumberOfArgs(String[] args) {
        if (args.length < 4) {
            String execName;
            execName = new File(MiningExperiment.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
            System.out.println("Start with arguments: \"java " + execName
                               + " inputFilename outputFilename minimumFrequqencyThresholdFactor errorThresholdFactor [separator] [pruningInterval]\"");
            System.exit(1);
        }
    }
}
