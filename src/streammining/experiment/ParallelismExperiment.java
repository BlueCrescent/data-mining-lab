package streammining.experiment;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ParallelismExperiment {
    public static void main(String[] args) {
        checkNumberOfArgs(args);

        final String inputFilename = args[0];
        final String outputFilename = args[1];
        final String outputFilenameStats = args[1] + ".stats";
        final double supportThreshold = Double.parseDouble(args[2]);
        final double supportErrorThreshold = Double.parseDouble(args[3]);
        final String separator = args.length < 6 ? "," : args[4];

        final List<List<Integer>> itemsets = ExperimentHelpers.readInput(inputFilename, separator);

        System.out.println("Running Parallelsim tests for file \"" + inputFilename + "\" with support threshold factor = " + args[2]
                           + " and  support error threshold factor = " + args[3] + ".");

        System.out.println("  Running with 1 thread...");
        final ExperimentStatistics algoStats1 = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 1, true);
        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename + ".1" }), outputFilenameStats, separator, algoStats1, false);

        System.out.println("  Running with 2 threads...");
        final ExperimentStatistics algoStats2 = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 2, true);
        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename + ".2" }), outputFilenameStats, separator, algoStats2, true);

        System.out.println("  Running with 3 threads...");
        final ExperimentStatistics algoStats3 = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 3, true);
        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename + ".3" }), outputFilenameStats, separator, algoStats3, true);

        System.out.println("  Running with 4 threads...");
        final ExperimentStatistics algoStats4 = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 4, true);
        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename + ".4" }), outputFilenameStats, separator, algoStats4, true);

        System.out.println("  Running with 5 threads...");
        final ExperimentStatistics algoStats5 = ExperimentHelpers.runAlgorithm(supportThreshold, supportErrorThreshold, itemsets, 5, true);
        ExperimentHelpers.printResults(Arrays.asList(new String[] { outputFilename + ".5" }), outputFilenameStats, separator, algoStats5, true);
    }

    private static void checkNumberOfArgs(String[] args) {
        if (args.length < 4) {
            String execName;
            execName = new File(MiningExperiment.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
            System.out.println("Start with arguments: \"java " + execName
                               + " inputFilename outputFilename minimumFrequqencyThresholdFactor errorThresholdFactor [separator]\"");
            System.exit(1);
        }
    }
}
