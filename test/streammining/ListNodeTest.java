package streammining;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ListNodeTest extends BaseNodeTest {

    ListNode<String> node1;
    ListNode<String> node2;

    @Override
    @BeforeEach
    public void setUp() {
        node2 = new ListNode<String>("b", 2);
        node1 = new ListNode<String>("a", 3, node2);
        node = node1;
    }

    @Test
    void testListNodeItemInt() {
        assertNull(node2.nextNode);
    }

    @Test
    void testListNodeItemIntListNodeOfItem() {
        assertEquals(node2, node1.nextNode);
    }

    @Test
    void testHasNextFalseIfNoNextNode() {
        assertFalse(node2.hasNext());
    }

    @Test
    void testHasNextTrueIfNextNodeSet() {
        assertTrue(node1.hasNext());
    }

    @Test
    void testGetNextNodeReturnsNextNode() {
        assertEquals(node2, node1.getNextNode());
    }

    @Test
    void testGetNextNodeReturnsNullIfNotSet() {
        assertEquals(null, node2.getNextNode());
    }

    @Test
    void testSetNextNode() {
        final ListNode<String> node3 = new ListNode<String>("c", 4);
        node2.setNextNode(node3);
        assertEquals(node3, node2.getNextNode());
    }

    @Test
    void testIteratorHasNext() {
        final Iterator<ListNode<String>> iter = node1.iterator();
        assertTrue(iter.hasNext());
    }

    @Test
    void testIteratorStartsWithNextNode() {
        final Iterator<ListNode<String>> iter = node1.iterator();
        assertEquals(node2, iter.next());
    }

    @Test
    void testIteratorIterates() {
        final ListNode<String> node3 = new ListNode<String>("c", 4);
        node2.setNextNode(node3);
        final Iterator<ListNode<String>> iter = node1.iterator();
        iter.next();
        assertTrue(iter.hasNext());
        assertEquals(node3, iter.next());
    }

    @Test
    void testIteratorHasCorrectBehaviourAtEnd() {
        final Iterator<ListNode<String>> iter = node1.iterator();
        iter.next();
        assertFalse(iter.hasNext());
        assertThrows(NoSuchElementException.class, () -> {
            iter.next();
        });
    }
}
