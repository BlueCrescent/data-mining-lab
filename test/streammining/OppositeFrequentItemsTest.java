package streammining;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OppositeFrequentItemsTest {

    private OppositeFrequentItems<String> ofiList;
    private ListNode<String> node;

    @BeforeEach
    void setUp() {
        ofiList = new OppositeFrequentItems<String>();
        node = new ListNode<String>("a", 2);
    }

    @Test
    void testContainsFalseIfNotAdded() {
        assertFalse(ofiList.contains("a"));
    }

    @Test
    void testContainsTrueAfterAddEntry() {
        ofiList.addEntry("a", 2, node);
        assertTrue(ofiList.contains("a"));
    }

    @Test
    void testSupportOneAfterAddEntry() {
        ofiList.addEntry("a", 2, node);
        assertEquals(1, ofiList.getSupport("a"));
    }

    @Test
    void testSupportTwoAfterAddEntryTwice() {
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("a", 3, node);
        assertEquals(2, ofiList.getSupport("a"));
    }

    @Test
    void testGetMaximalSetOfFrequentItemsReturnsSingleAddedNodeIfFrequent() {
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("a", 3, node);
        assertEquals("a", ofiList.getMaximalSetOfFrequentItems(2).get(0).getItem());
    }

    @Test
    void testGetMaximalSetOfFrequentItemsReturnsNothingIfAddedNodeInfrequent() {
        ofiList.addEntry("a", 2, node);
        assertEquals(0, ofiList.getMaximalSetOfFrequentItems(2).size());
    }

    @Test
    void testWindowIdDoesNotChangeAfterFirstAddingNode() {
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("a", 3, node);
        assertEquals(2, ofiList.getMaximalSetOfFrequentItems(2).get(0).getWindowId());
    }

    @Test
    void testAddingNodeOfExistingItemSetsCorrectLinksBetweenNodes() {
        final ListNode<String> node2 = new ListNode<String>("a", 3);
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("a", 3, node2);
        final ListNode<String> queryNode = ofiList.getMaximalSetOfFrequentItems(2).get(0);
        assertEquals(node2, queryNode.getNextNode());
        assertEquals(node, node2.getNextNode());
    }

    @Test
    void testGetMaximalSetOfFrequentItemsReturnsSortedItems() {
        final ListNode<String> node2 = new ListNode<String>("b", 3);
        final ListNode<String> node3 = new ListNode<String>("c", 3);
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("c", 3, node3);
        ofiList.addEntry("b", 3, node2);
        final List<ListNode<String>> items = ofiList.getMaximalSetOfFrequentItems(1);
        assertEquals("a", items.get(0).getItem());
        assertEquals("b", items.get(1).getItem());
        assertEquals("c", items.get(2).getItem());
    }

    @Test
    void testGetMaximalSetOfFrequentItemsDoesNotReturnInfrequentItem() {
        final ListNode<String> node2 = new ListNode<String>("b", 3);
        final ListNode<String> node3 = new ListNode<String>("a", 3);
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("b", 3, node2);
        ofiList.addEntry("a", 3, node3);
        final List<ListNode<String>> items = ofiList.getMaximalSetOfFrequentItems(2);
        assertEquals(1, items.size());
        assertEquals("a", items.get(0).getItem());
    }

    @Test
    void testAddingNodesForDifferentItemsWorks() {
        final ListNode<String> node2 = new ListNode<String>("b", 3);
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("b", 3, node2);
        final List<ListNode<String>> items = ofiList.getMaximalSetOfFrequentItems(1);
        assertEquals(items.get(0).getItem(), "a");
        assertEquals(items.get(1).getItem(), "b");
        assertEquals(items.get(0).getSupport(), 1);
        assertEquals(items.get(1).getSupport(), 1);
        assertEquals(items.get(0).getWindowId(), 2);
        assertEquals(items.get(1).getWindowId(), 3);
        assertEquals(items.get(0).getNextNode(), node);
        assertEquals(items.get(1).getNextNode(), node2);
    }

    @Test
    void testDeleteItemRemovesCorrectItemAndReturnsIt() {
        final ListNode<String> node2 = new ListNode<String>("b", 3);
        final ListNode<String> node3 = new ListNode<String>("c", 3);
        ofiList.addEntry("a", 2, node);
        ofiList.addEntry("c", 3, node3);
        ofiList.addEntry("b", 3, node2);
        final ListNode<String> deletedNode = ofiList.deleteItem("c");
        final List<ListNode<String>> items = ofiList.getMaximalSetOfFrequentItems(1);
        assertEquals(2, items.size());
        assertEquals("a", items.get(0).getItem());
        assertEquals("b", items.get(1).getItem());
        assertEquals("c", deletedNode.getItem());
        assertEquals(node3, deletedNode.getNextNode());
    }

}
