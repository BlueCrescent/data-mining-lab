package streammining;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FrequentItemListEntryTest {

    private static final List<String> itemset1 = asList("a", "b", "c");
    private FrequentItemListEntry<String> listEntry;
    private Set<List<String>> itemsetStorage;

    @BeforeEach
    void setUp() throws Exception {
        listEntry = new FrequentItemListEntry<String>(itemset1, 2);
    }

    @Test
    void testGetItemReturnsPrimaryItem() {
        assertEquals("a", listEntry.getItem());
    }

    @Test
    void testGetSupportReturnsOneIfOnlyOneItemsetAdded() {
        assertEquals(1, listEntry.getSupport());
    }

    @Test
    void testGetSupportIncreasesWhenMoreItemsetsAdded() {
        listEntry.addItemset(asList("a", "c"), 2);
        assertEquals(2, listEntry.getSupport());
        listEntry.addItemset(asList("a", "b", "d"), 2);
        assertEquals(3, listEntry.getSupport());
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsSingleAddedSet() {
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsTwoItemsetsWithLastItemDiffering() {
        final List<String> itemset2 = asList("a", "b", "d");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertContainsExactlyMaximalItemsets(asList("a", "b", "c"), asList("a", "b", "d"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsOnlySupportedSubItemsets() {
        final List<String> itemset2 = asList("a", "b", "d");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "b"));
        assertContainsExactlyMaximalItemsets(asList("a", "b"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsOnlySupportedSubItemsetsInMultiplePaths() {
        final List<String> itemset2 = asList("a", "c");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "c"));
        assertContainsExactlyMaximalItemsets(asList("a", "c"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsItemsetAddedMultipleTimesAsFrequent() {
        final List<String> itemset2 = asList("a", "b", "c");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "b", "c"));
        assertContainsExactlyMaximalItemsets(asList("a", "b", "c"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsOnlyFirstItemIfNoOtherIsFrequent() {
        final List<String> itemset2 = asList("a", "d", "e");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a"));
        assertContainsExactlyMaximalItemsets(asList("a"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsMultipleFrequentSubsetsWhileSupersetsInfrequent() {
        final List<String> itemset2 = asList("a", "b", "d");
        final List<String> itemset3 = asList("a", "c", "d");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(3, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "b"), asList("a", "c"), asList("a", "d"));
    }

    @Test
    void testGetMaximalFrequentItemsetsReturnsMultipleFrequentSubsetsWhileSupersetsInfrequent2() {
        final List<String> itemset2 = asList("a", "b", "d");
        final List<String> itemset3 = asList("a", "c", "d");
        final List<String> itemset4 = asList("a", "b", "c", "d");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        listEntry.addItemset(itemset4, 3);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(3, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "b", "c"), asList("a", "c", "d"), asList("a", "b", "d"));
    }

    @Test
    void testGetMaximalFrequentItemsetsWithAddedItemsetThatAreSubsetsOfOtherOne() {
        final List<String> itemset2 = asList("a", "b", "c", "d");
        listEntry.addItemset(itemset2, 2);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "b", "c"));
    }

    @Test
    void testGetMaximalFrequentWithSomeMaximalItemsetsBeingFrequentAndSomeSubsets() {
        final List<String> itemset2 = asList("a", "b", "d");
        final List<String> itemset3 = asList("a", "c", "e");
        final List<String> itemset4 = asList("a", "b", "c", "e");
        final List<String> itemset5 = asList("a", "d", "e");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        listEntry.addItemset(itemset4, 3);
        listEntry.addItemset(itemset5, 5);
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertContainsExactlyMaximalItemsets(asList("a", "b", "c"), asList("a", "c", "e"), asList("a", "d"));
    }

    @Test
    void testRemoveItemAtEndIfContainsSingleItemset() {
        listEntry.removeItem("c");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "b"));
    }

    @Test
    void testRemoveItemInMiddleIfContainsSingleItemset() {
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "c"));
    }

    @Test
    void testRemoveItemWithMultipleChildren() {
        final List<String> itemset2 = asList("a", "b", "d", "e");
        listEntry.addItemset(itemset2, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(2, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "c"), asList("a", "d", "e"));
    }

    @Test
    void testRemoveItemWithMultipleChildrenAndUnrelatedSiblingsInParentNode() {
        final List<String> itemset2 = asList("a", "b", "d", "e");
        final List<String> itemset3 = asList("a", "f");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertContainsExactlyMaximalItemsets(asList("a", "c"), asList("a", "f"), asList("a", "d", "e"));
    }

    @Test
    void testRemoveItemWithMultipleChildrenAndRelatedSiblingsInParentNode1() {
        final List<String> itemset2 = asList("a", "c", "d");
        listEntry.addItemset(itemset2, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(1, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "c", "d"));
    }

    @Test
    void testRemoveItemWithMultipleChildrenAndRelatedSiblingsInParentNode2() {
        final List<String> itemset2 = asList("a", "c", "d");
        listEntry.addItemset(itemset2, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "c"));
    }

    @Test
    void testRemoveItemWithMultipleChildrenAndRelatedSiblingsInParentNodeWithChildsChildrenAlsoRequiringMerge1() {
        final List<String> itemset2 = asList("a", "b", "c", "d");
        final List<String> itemset3 = asList("a", "c", "d", "e");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(2);
        assertEquals(1, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "c", "d"));
    }

    @Test
    void testRemoveItemWithMultipleChildrenAndRelatedSiblingsInParentNodeWithChildsChildrenAlsoRequiringMerge2() {
        final List<String> itemset2 = asList("a", "b", "c", "d");
        final List<String> itemset3 = asList("a", "c", "d", "e");
        listEntry.addItemset(itemset2, 2);
        listEntry.addItemset(itemset3, 2);
        listEntry.removeItem("b");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(3);
        assertEquals(1, itemsetStorage.size());
        assertContainsExactlyMaximalItemsets(asList("a", "c"));
    }

    @Test
    void testRemovingItemThatDoesNotExistDoesNotChangeAnything() {
        listEntry.removeItem("d");
        itemsetStorage = listEntry.getMaximalFrequentItemsets(1);
        assertEquals(1, itemsetStorage.size());
        assertThat(getItem(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    private static List<String> asList(String... strings) {
        return Arrays.asList(strings);
    }

    @SafeVarargs
    private final void assertContainsExactlyMaximalItemsets(List<String>... expectedItemsets) {
        assertContainsMaximalItemsets(expectedItemsets);
        assertContainsNoOtherMaximalItemsetsThan(expectedItemsets);
    }

    @SafeVarargs
    private final void assertContainsMaximalItemsets(List<String>... expectedItemsets) {
        assertThat(itemsetStorage, CoreMatchers.hasItems(expectedItemsets));
        for (final List<String> expectedItemset : expectedItemsets)
            assertTrue(containsSuperset(expectedItemset, itemsetStorage));
    }

    @SafeVarargs
    private final void assertContainsNoOtherMaximalItemsetsThan(List<String>... expectedItemsets) {
        for (final List<String> itemset : itemsetStorage)
            assertTrue(containsSuperset(itemset, Arrays.asList(expectedItemsets)));
    }

    private List<String> getItem(int i) {
        int j = 0;
        for (final List<String> itemset : itemsetStorage) {
            if (j == i)
                return itemset;
            ++j;
        }
        return null;
    }

    private static boolean containsSuperset(List<String> ofItemset, Iterable<List<String>> inItemsetList) {
        for (final List<String> itemset : inItemsetList)
            if (itemset.containsAll(ofItemset))
                return true;
        return false;
    }
}
