package streammining;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Collection;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TreeNodeTest extends ListNodeTest {

    TreeNode<String> node2t;

    @Override
    @BeforeEach
    public void setUp() {
        final TreeNode<String> node1t = new TreeNode<String>("a", 3);
        node = node1t;
        node1 = node1t;
        node2t = new TreeNode<String>("b", 2);
        node2 = node2t;
        node1t.setNextNode(node2);

    }

    @Test
    void testTreeNode() {
        assertTrue(node2t.children.isEmpty());
    }

    @Test
    void testHasChildrenFalseIfNoneAdded() {
        assertEquals(false, node2t.hasChildren());
    }

    @Test
    void testHasChildrenTrueAfterAddingChild() {
        node2t.addChild("c", 4);
        assertEquals(true, node2t.hasChildren());
    }

    @Test
    void testHasChildForItemFalseIfNoneAdded() {
        assertEquals(false, node2t.hasChildForItem("d"));
    }

    @Test
    void testHasChildForItemFalseIfOnlyOthersAdded() {
        node2t.addChild("c", 3);
        node2t.addChild("e", 6);
        assertEquals(false, node2t.hasChildForItem("d"));
    }

    @Test
    void testHasChildForItemTrueAfterAddingChildForItem() {
        node2t.addChild("c", 3);
        node2t.addChild("d", 6);
        assertEquals(true, node2t.hasChildForItem("d"));
    }

    @Test
    void testAddingMultipleChildrenForSameItemDoesNotIncreseChildCount() {
        node2t.addChild("c", 3);
        node2t.addChild("c", 6);
        assertEquals(1, node2t.getChildren().size());
    }

    @Test
    void testAddingMultipleChildrenForSameItemDoesIncrementSupportOfChild() {
        node2t.addChild("c", 3);
        node2t.addChild("c", 6);
        assertEquals(2, node2t.getChildForItem("c").getSupport());
    }

    @Test
    void testGetChildrenEmptyIfNoneAdded() {
        assertTrue(node2t.getChildren().isEmpty());
    }

    @Test
    void testGetChildrenContainsAddedChildren() {
        node2t.addChild("c", 3);
        node2t.addChild("d", 6);
        final Collection<TreeNode<String>> children = node2t.getChildren();
        assertFalse(node2t.getChildren().isEmpty());
        assertEquals(2, children.size());
        assertThat(node2t.children.keySet(), CoreMatchers.hasItems("c", "d"));
        assertChildrenContainExpectedNodes(children);
    }

    private void assertChildrenContainExpectedNodes(Collection<TreeNode<String>> children) {
        @SuppressWarnings("unchecked")
        final TreeNode<String>[] childrenArray = children.toArray((TreeNode<String>[]) new TreeNode<?>[0]);
        if (childrenArray[0].getItem() == "c") {
            assertEquals(3, childrenArray[0].getWindowId());
            assertEquals("d", childrenArray[1].getItem());
            assertEquals(6, childrenArray[1].getWindowId());
        } else if (childrenArray[0].getItem() == "d") {
            assertEquals(6, childrenArray[0].getWindowId());
            assertEquals("c", childrenArray[1].getItem());
            assertEquals(3, childrenArray[1].getWindowId());
        } else {
            fail("Unexpected item encountered.");
        }
    }

    @Test
    void testAddChildReturnsCorrestTreeNode() {
        final TreeNode<String> c = node2t.addChild("c", 3);
        assertEquals("c", c.getItem());
        assertEquals(1, c.getSupport());
        assertEquals(3, c.getWindowId());
    }

    @Test
    void testAddChildDoesNotAddNewNodeForExistingItem() {
        final TreeNode<String> c1 = node2t.addChild("c", 3);
        final TreeNode<String> c2 = node2t.addChild("c", 5);
        assertEquals(c1, c2);
        assertEquals(1, node2t.getChildren().size());
    }

    @Test
    void testAddChildIncrementsSupportOfExistingChild() {
        final TreeNode<String> c1 = node2t.addChild("c", 3);
        assertEquals(1, c1.getSupport());
        final TreeNode<String> c2 = node2t.addChild("c", 5);
        assertEquals(2, c2.getSupport());
    }

    @Test
    void testMergeAddsSupportCorrectly() {
        final TreeNode<String> node3t = new TreeNode<String>("b", 4);
        node2t.incrementSupport();
        node3t.incrementSupport();
        node3t.incrementSupport();
        node2t.merge(node3t);
        assertEquals(5, node2t.getSupport());
    }

    @Test
    void testMergeChoosesCorrectWindowIdFromFirstNode() {
        final TreeNode<String> node3t = new TreeNode<String>("b", 4);
        node2t.merge(node3t);
        assertEquals(2, node2t.getWindowId());
    }

    @Test
    void testMergeChoosesCorrectWindowIdFromSecondNode() {
        final TreeNode<String> node3t = new TreeNode<String>("b", 1);
        node2t.merge(node3t);
        assertEquals(1, node2t.getWindowId());
    }

    @Test
    void testMergeAddsNewChild() {
        final TreeNode<String> node3t = new TreeNode<String>("b", 4);
        node2t.addChild("c", 2);
        node3t.addChild("d", 4);
        node2t.merge(node3t);
        assertEquals(2, node2t.getChildren().size());
        assertThat(node2t.children.keySet(), CoreMatchers.hasItems("c", "d"));
    }

    @Test
    void testMergeMergesExistingChild() {
        final TreeNode<String> node3t = new TreeNode<String>("b", 4);
        node2t.addChild("c", 2);
        node2t.addChild("d", 3);
        node3t.addChild("d", 4);
        node2t.merge(node3t);
        assertEquals(2, node2t.getChildren().size());
        assertThat(node2t.children.keySet(), CoreMatchers.hasItems("c", "d"));
        assertEquals(2, node2t.getChildForItem("d").getSupport());
        assertEquals(3, node2t.getChildForItem("d").getWindowId());
    }

    @Test
    void testRemoveDeletesNodeFromParentsChildren() {
        final TreeNode<String> node3t = node2t.addChild("b", 2);
        node3t.remove();
        assertEquals(0, node2t.getChildren().size());
    }

    @Test
    void testRemoveTransfresChildrenToParent() {
        final TreeNode<String> node3t = node2t.addChild("b", 2);
        node2t.addChild("c", 2).incrementSupport();
        node3t.addChild("c", 3);
        node3t.addChild("d", 4);
        node3t.remove();
        assertEquals(2, node2t.getChildren().size());
        assertThat(node2t.children.keySet(), CoreMatchers.hasItems("c", "d"));
        assertEquals(3, node2t.getChildForItem("c").getSupport());
        assertEquals(2, node2t.getChildForItem("c").getWindowId());
    }

    @Test
    void testRemoveSetsParentForTransferedChildren() {
        final TreeNode<String> node3t = node2t.addChild("b", 2);
        node2t.addChild("c", 2);
        node3t.addChild("c", 3);
        node3t.addChild("d", 4);
        node3t.remove();
        assertEquals(node2t, node2t.getChildForItem("c").parent);
        assertEquals(node2t, node2t.getChildForItem("d").parent);
    }
}
