package streammining;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StreamMiningAlgorithmTest {

    private StreamMiningAlgorithm<Integer> algo;

    @BeforeEach
    void setUp() throws Exception {
        algo = new StreamMiningAlgorithm<Integer>();
    }

    @Test
    void testEmptyAlgorithm() {
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(0, itemsets.size());
    }

    @Test
    void testSingleItemset_FrequentViaWindowId() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 2);
        assertEquals(1, itemsets.size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3));
    }

    @Test
    void testSingleItemset_FrequentViaSupportThreshold() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3));
    }

    @Test
    void testSingleItemset_SemiFrequent() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0.5, 4);
        assertEquals(1, itemsets.size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3));
    }

    @Test
    void testSingleItemset_Infrequent() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(0, itemsets.size());
    }

    @Test
    void testSingleItemset_InfrequentOverWholeStream() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 2, true);
        assertEquals(0, itemsets.size());
    }

    @Test
    void testSingleItemset_InfrequentOverWholeStreamWithSupportThreshold() {
        algo.addItemset(asList(1, 2, 3), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4, true);
        assertEquals(0, itemsets.size());
    }

    @Test
    void testInfrequentItemsetAndFrequentSubItemset1() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 3), 3);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 3));
    }

    @Test
    void testInfrequentItemsetAndFrequentSubItemset2() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(2, 3), 3);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(2, 3));
    }

    @Test
    void testInfrequentItemsetAndFrequentSubItemset3() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 2), 3);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2));
    }

    @Test
    void testFrequentSubItemsetsThroughIntersectionWithOtherInfrequentSubItemsets1() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 2, 4), 2);
        algo.addItemset(asList(1, 3, 5), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(2, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertEquals(2, itemsets.get(1).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2));
        assertThat(itemsets.get(1), CoreMatchers.hasItems(1, 3));
    }

    @Test
    void testFrequentSubItemsetsThroughIntersectionWithOtherInfrequentSubItemsets2() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(2, 4), 2);
        algo.addItemset(asList(3, 5), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(1., 0., 4);
        assertEquals(2, itemsets.size());
        assertEquals(1, itemsets.get(0).size());
        assertEquals(1, itemsets.get(1).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(2));
        assertThat(itemsets.get(1), CoreMatchers.hasItems(3));
    }

    @Test
    void testSingleInfrequentItemsetGetsPruned() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.prune(0.6, 4);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0., 0., 4);
        assertEquals(0, itemsets.size());
    }

    @Test
    void testSingleInfrequentItemsetDoesNotGetPrunedIfErrorThresholdToLow() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.prune(0.5, 4);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
    }

    @Test
    void testInfrequentSubItemsetGetsPruned1() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 3), 2);
        algo.prune(0.6, 4);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 3));
    }

    @Test
    void testInfrequentSubItemsetGetsPruned2() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(2, 3), 2);
        algo.prune(0.6, 4);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(2, 3));
    }

    @Test
    void testInfrequentSubItemsetGetsPruned3() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 2), 2);
        algo.prune(0.6, 4);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
        assertEquals(2, itemsets.get(0).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2));
    }

    @Test
    void testMinSupportZeroDoesNotConfuseAlgorithm() {
        algo.addItemset(asList(1, 2, 3), 2);
        algo.addItemset(asList(1, 2, 4), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0., 0., 4);
        assertEquals(2, itemsets.size());
        assertEquals(3, itemsets.get(0).size());
        assertEquals(3, itemsets.get(1).size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3));
        assertThat(itemsets.get(1), CoreMatchers.hasItems(1, 2, 4));
    }

    @Test
    void testParallelOperations_SingleItemset() {
        algo = new StreamMiningAlgorithm<Integer>(2);
        algo.addItemset(asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20), 2);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 4);
        assertEquals(1, itemsets.size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));
    }

    @Test
    void testParallelOperations_MultipleItemsets() {
        algo = new StreamMiningAlgorithm<Integer>(2);
        algo.addItemset(asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20), 1);
        algo.addItemset(asList(1, 2, 3, 4), 1);
        algo.addItemset(asList(17, 18, 19, 20), 1);
        final List<List<Integer>> itemsets = algo.getFrequentItemsets(0.5, 0., 5);
        assertEquals(2, itemsets.size());
        assertThat(itemsets.get(0), CoreMatchers.hasItems(1, 2, 3, 4));
        assertThat(itemsets.get(1), CoreMatchers.hasItems(17, 18, 19, 20));
    }

    @SafeVarargs
    private static <T> List<T> asList(T... objects) {
        return Arrays.asList(objects);
    }

}
