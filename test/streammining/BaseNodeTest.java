package streammining;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BaseNodeTest {

    BaseNode<String> node;

    @BeforeEach
    public void setUp() {
        node = new BaseNode<String>("a", 3);
    }

    @Test
    void testGetItem() {
        assertEquals(node.getItem(), "a");
    }

    @Test
    void testGetSupport() {
        assertEquals(node.getSupport(), 1);
    }

    @Test
    void testGetWindowId() {
        assertEquals(node.getWindowId(), 3);
    }

    @Test
    void testIncrementSupport() {
        node.incrementSupport();
        node.incrementSupport();
        assertEquals(node.getSupport(), 3);
    }
}
