package streammining;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MaximalFrequentItemsetListTest {

    private MaximalFrequentItemsetList<String> list;

    @BeforeEach
    void setUp() throws Exception {
        list = new MaximalFrequentItemsetList<String>();
    }

    @Test
    void testAddingSingleItemset() {
        list.add(asList("a", "b", "c"));
        assertEquals(1, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @Test
    void testAddingTwoUnrelatedItemsets() {
        list.add(asList("a", "b", "c"));
        list.add(asList("d", "e"));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalFirstItem() {
        list.add(asList("a", "b", "c"));
        list.add(asList("a", "d", "e"));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("a", "d", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalMiddleItem() {
        list.add(asList("a", "b", "c"));
        list.add(asList("d", "b", "e"));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "b", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalLastItem() {
        list.add(asList("a", "b", "c"));
        list.add(asList("d", "e", "c"));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "e", "c"));
    }

    @Test
    void testAddingSameItemsetTwiceChangesNothing() {
        list.add(asList("a", "b", "c"));
        assertFalse(list.add(asList("a", "b", "c")));
        assertEquals(1, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @Test
    void testAddingSubsetChangesNothing() {
        list.add(asList("a", "b", "c"));
        assertFalse(list.add(asList("b", "c")));
        assertEquals(1, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @Test
    void testAddingTwoUnrelatedItemsetsViaAddAll() {
        list.addAll(asList(asList("a", "b", "c"), asList("d", "e")));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalFirstItemViaAddAll() {
        list.addAll(asList(asList("a", "b", "c"), asList("a", "d", "e")));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("a", "d", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalMiddleItemViaAddAll() {
        list.addAll(asList(asList("a", "b", "c"), asList("d", "b", "e")));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "b", "e"));
    }

    @Test
    void testAddingDifferentItemsetsWithIdenticalLastItemViaAddAll() {
        list.addAll(asList(asList("a", "b", "c"), asList("d", "e", "c")));
        assertEquals(2, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
        assertThat(list.get(1), CoreMatchers.hasItems("d", "e", "c"));
    }

    @Test
    void testAddingSameItemsetTwiceViaAddAllChangesNothing() {
        list.addAll(asList(asList("a", "b", "c"), asList("a", "b", "c")));
        assertEquals(1, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @Test
    void testAddingSubsetViaAddAllChangesNothing() {
        list.addAll(asList(asList("a", "b", "c"), asList("b", "c")));
        assertEquals(1, list.size());
        assertThat(list.get(0), CoreMatchers.hasItems("a", "b", "c"));
    }

    @SafeVarargs
    private static <T> List<T> asList(T... objects) {
        return Arrays.asList(objects);
    }

}
