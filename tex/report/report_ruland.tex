\documentclass{scrartcl} 

%common packages
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{algorithm}
\usepackage[noend]{algorithmic}
\usepackage{natbib}

%some common comman definitions
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\expected}{\mathbb{E}}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\smallo}{\textit{\textbf{o}}}

\usepackage{graphicx} % Include graphics.
\usepackage{graphics}
\usepackage{tikz}
\usetikzlibrary{matrix,positioning}
\usepackage{float} % Fixe Bildpositionen festlegen
\usepackage[export]{adjustbox} % Linksbuendig etc.
\usepackage{wrapfig} % wrapping figures in text

\usepackage[none]{hyphenat} %keine automatische Zerlegung von Worten
\sloppy   % kein Zeilenueberlauf FIXME use sloppypar (what is that?)

\usepackage{tabularx} % tables
\usepackage{multirow}

\title{Analysis of the DSM-FI algorithm}
\subtitle{Lab Report: Development and Application of Data Mining and Learning Systems: Machine Learning and Data Mining 2018}
\date{September 25, 2018}

\author{
Timm Ruland
}

\begin{filecontents}{refs.bib}
	
	@article{li2008dsm,
		title={DSM-FI: an efficient algorithm for mining frequent itemsets in data streams},
		author={Li, Hua-Fu and Shan, Man-Kwan and Lee, Suh-Yin},
		journal={Knowledge and Information Systems},
		volume={17},
		number={1},
		pages={79--97},
		year={2008},
		publisher={Springer}
	}

	 @article{applicationsSurvey,
		author = {Kumar, Dr K Ramesh},
		year = {2014},
		month = {04},
		pages = {264-275},
		title = {A complete survey on application of frequent pattern mining and association rulemining on crime pattern mining},
		volume = {3},
		booktitle = {International journal of Advances in Computer Science and Technology}
	}

	@article{Agrawal:1993:MAR:170036.170072,
		author = {Agrawal, Rakesh and Imieli\'{n}ski, Tomasz and Swami, Arun},
		title = {Mining Association Rules Between Sets of Items in Large Databases},
		journal = {SIGMOD Rec.},
		issue_date = {June 1, 1993},
		volume = {22},
		number = {2},
		month = jun,
		year = {1993},
		issn = {0163-5808},
		pages = {207--216},
		numpages = {10},
		url = {http://doi.acm.org/10.1145/170036.170072},
		doi = {10.1145/170036.170072},
		acmid = {170072},
		publisher = {ACM},
		address = {New York, NY, USA},
	}

	@inproceedings{brijs99using,
		author = {Tom Brijs and Gilbert Swinnen and Koen Vanhoof and Geert Wets},
		title = {Using Association Rules for Product Assortment Decisions: A Case Study},
		booktitle = {Knowledge Discovery and Data Mining},
		pages = {254-260},
		year = {1999},
	}

	@article{sun2006finding,
		title={Finding frequent itemsets in high-speed data streams},
		author={Sun, Xingzhi and Orlowska, Maria E and Li, Xue},
		year={2006},
		publisher={Citeseer}
	}

	@article{dang2008online,
		title={Online mining of frequent sets in data streams with error guarantee},
		author={Dang, Xuan Hong and Ng, Wee-Keong and Ong, Kok-Leong},
		journal={Knowledge and information systems},
		volume={16},
		number={2},
		pages={245--258},
		year={2008},
		publisher={Springer}
	}

	@inproceedings{trabold2016mining,
		title={Mining Data Streams with Dynamic Confidence Intervals},
		author={Trabold, Daniel and Horv{\'a}th, Tam{\'a}s},
		booktitle={International Conference on Big Data Analytics and Knowledge Discovery},
		pages={99--113},
		year={2016},
		organization={Springer}
	}
\end{filecontents}

\begin{document}

\maketitle

\begin{abstract}
A fundamental task in machine learning is the mining of frequent itemsets.
It has many real world applications which often are online problems where new data is produced continuously. 
%Many real world applications are online problems which continuously produce new data. 
Multiple algorithms were proposed to tackle this challenge efficiently. 
In this lab, we evaluated one of them, the DSM-FI algorithm.
\end{abstract}

\section{Introduction}
%State the problem and introduce your approach. (see paper-structure.pdf for details)
Frequent itemset mining is an important task in knowledge discovery. 
It handles settings where data points called \textit{items} occur as a collection of sets called \textit{transactions}. 
The goal is to identify subsets of them which occur frequently, i.e. in a minimum percentage of all transactions. 
This can be utilized in a variety of applications over different fields such as economics, physics, medicine and crime prevention, \cite{applicationsSurvey}. 
A well-known example is the analysis of purchase behavior (market basket analysis, \cite{Agrawal:1993:MAR:170036.170072}). 
Here, an itemset is the set of products bought by a single customer. 
It is investigated which items are often bought together. 
This can be used for optimizing advertisements and the placement of products. 

Additionally, frequent itemset mining is the basis for association rule mining which adds the study of causal connections between the itemsets. 

Many such problems occur as online problems where the transactions occur in an ongoing stream. 
The goal is to be able to query the frequent itemsets of the current stream at any given point in time. 
This means that items which were not frequent before or did not occur at all, may become frequent at a later point in time or that frequent items become infrequent when they stop appearing while the size of the stream grows.
Additionally, the ever growing size of the stream can make storing all its content infeasible. 
The transactions are only seen once and must be processed at that time. 

In this lab, we implemented and evaluated the algorithm DSM-FI (data stream mining for frequent itemsets) by \cite{li2008dsm}. 
It uses a prefix tree based data structure to summarize the content of the stream. 
For the evaluation, we analyse the runtime and memory costs of the algorithm and compare it to other algorithms. 
We first give a formal definition of itemset mining in \autoref{section:problemDefinition}. 
%Then we give a brief overview over the comparison algorithms. 
In \autoref{section:algorithm} we describe the DSM-FI algorithm and its data structure. 
The evaluation and its results are described in \autoref{section:evaluation}. 
And finally, our concluding remarks are given in \autoref{section:conclusion}.

\section{Problem Definition}
\label{section:problemDefinition}

The \textit{items} in the frequent itemset mining task are some literals $\Psi = \{i_1, ..., i_n\}$. 
They are typically integers or strings. 
Non-empty subsets $I\subset \Psi$ are called \textit{itemsets}. 
Those occur in \textit{transactions} $T = \left\langle tid, \left( x_1,...,x_q\right)\right\rangle$ where $x_i \in \Psi$ and $tid$ is a unique \textit{transaction id}.
For a set of transactions $\mathcal{T}$, the \textit{support} of an itemset $I$ is the number of transactions in $\mathcal{T}$ which contain $I$ as a subset.
An itemset is \textit{frequent} if its support is at least as large as $s \cdot \left| \mathcal{T} \right|$ for $s \in \left(0,1\right)$, i.e. it is contained in a certain fraction of all transactions. 
Otherwise, it is called \textit{infrequent}.
The value $s \in \left(0,1\right)$ is called the \textit{minimum support threshold}.
Such an itemset is \textit{maximal} if it has no frequent superset in $\mathcal{T}$.
Overall, the basic \textit{frequent itemset mining} task is to find all (maximal) frequent itemsets in a given set of transactions $\mathcal{T}$.

This can be further modified as an online problem. 
Here, the transactions are given as an ongoing stream $DS=\left[T_1,T_2,...,T_N\right)$. 
The size $N$ of the the stream denotes the "current" number of transactions received from the stream and is growing. 
The goal is to process each transaction as soon as it is received and to be able to query the frequent itemsets of the current stream at any point.

Additionaly, the problem can be relaxed by introudcing \textit{semi-frequent} itemsets. 
Those are infreqeunt itemsets for which the support still surpasses $\varepsilon\cdot \left| \mathcal{T} \right|$ with $\varepsilon\in \left(0,s\right)$ a given \textit{maximum support error threshold}. 
A valid solution of the relaxed problem is allowed (but not required) to contain some of the semi-frequent itemsets. 

%\section{Related Work}
%Here you can describe papers that relate to your work. Even though the related work section is very common, it is also acceptable to describe the related work whenever it fits, e.g., in the introduction or in the description of your approach and leave out this section entirely. The decision is up to you. For citations you can use the natbib-commands citet for citations in the text (i.e., use the citation as a noun) and citep for parenthetical citations (i.e., the citation is in brackets). (see paper-structure.pdf for details)

\section{DSM-FI}
\label{section:algorithm}
The DSM-FI algorithm presented by \cite{li2008dsm} tries to tackle the stream mining problem by proposing a summarizing data structure for the stream that can be updated and queried continuously. 
It requires an order on the items to be given (e.g. lexicographically) and starts with sorting any given itemset. 
The data structure consists of a collection of prefix trees where each one uniquely belongs to one of the items encountered so far. 
The nodes in the tree correspond to items and additionally store a support value. 
The root of each tree contains the item for the respective tree. 
The trees are generated by adding all (sorted) itemsets to it that start with that item. 
Hereby, a path following the items in the set is added. 
If a prefix of the set is already encoded in the tree, instead the support in that part of the path is incremented. 

As an example, consider the itemsets $(a,b,c)$, $(a,b,c,d)$, $(a,b,c,f)$, $(a,c,d,f)$ and $(a,c,f)$. 
The initial tree for the first itemset $(a,b,c)$ is the simple path shown in \autoref{figure:prefix_tree} a).
The next itemset $(a,b,c,d)$ contains the first one as a prefix. 
Therefore, the support in the corresponding nodes is incremented and the additional node is appended with support one in \autoref{figure:prefix_tree} b). 
The third itemset again has the first one as a prefix but ends with a different item. 
Therefore, the new path forks off the existing one in \autoref{figure:prefix_tree} c). 
After adding the remaining itemsets, the tree in \autoref{figure:prefix_tree} d) is obtained. 

\begin{figure}[t]%$[H]%[htbp]
%		\centering
	\begin{minipage}{.16\linewidth}
		a)
	\end{minipage}
	\begin{minipage}{.16\linewidth}
		b)
	\end{minipage}
	\begin{minipage}{.16\linewidth}
		c)
	\end{minipage}
	\begin{minipage}{.16\linewidth}
		d)
	\end{minipage}
\\
	\begin{minipage}{.16\linewidth}
		%		\begin{table}
%					\centering
%		\scalebox{0.8}{
			\begin{figure}[H]%[htbp]
				\centering
				\begin{tikzpicture}[level/.style={sibling distance=10em/#1},
				every node/.style = {shape=circle, rounded corners,
					draw, align=center,
					top color=white, bottom color=blue!20}]
				\node {a:1}
				child { node {b:1} 
					child { node {c:1}} };
				\end{tikzpicture}
%				\caption{Prefix tree for single itemsets (a,b,c).}
			\end{figure}
%		}
	\end{minipage}
	\begin{minipage}{.16\linewidth}
%		\scalebox{0.88}{
			\begin{figure}[H]%[htbp]
				\centering
				\begin{tikzpicture}[level/.style={sibling distance=10em/#1},
				every node/.style = {shape=circle, rounded corners,
					draw, align=center,
					top color=white, bottom color=blue!20}]
				\node {a:2}
				child { node {b:2} 
					child { node {c:2}
						child {node {d:1}} } };
				\end{tikzpicture}
%				\caption{Prefix tree for single itemsets (a,b,c) and (a,b,c,d).}
			\end{figure}
%		}
	\end{minipage}
	\begin{minipage}{.16\linewidth}
%		\scalebox{0.88}{
			\begin{figure}[H]%[htbp]
				\centering
				\begin{tikzpicture}[level/.style={sibling distance=10em/#1},
				every node/.style = {shape=circle, rounded corners,
					draw, align=center,
					top color=white, bottom color=blue!20}]
				\node {a:3}
				child { node {b:3} 
					child { node {c:3}
						child { node {d:1}}
						child { node {f:1}}}};
				\end{tikzpicture}
%				\caption{Prefix tree for itemsets (a,b,c), (a,b,c,d) and (a,b,c,f).}
			\end{figure}
%		}
	\end{minipage}
%	$ $ $ $ 
	\begin{minipage}{.16\linewidth}
%		\scalebox{0.88}{
			\begin{figure}[H]%[htbp]
				\centering
				\begin{tikzpicture}[level/.style={sibling distance=10em/#1},
				every node/.style = {shape=circle, rounded corners,
					draw, align=center,
					top color=white, bottom color=blue!20}]
				\node {a:5}
				child { node {b:3} 
					child { node {c:3}
						child { node {d:1}}
						child { node {f:1}}}}
				child { node {c:2}
					child { node {d:1}
						child { node {f:1}}}
					child { node {f:1}} };
				\end{tikzpicture}
%				\caption{Prefix tree for itemsets (a,b,c), (a,b,c,d), (a,b,c,f), (a,c,d,f) and (a,c,f).}
			\end{figure}
%		}
	\end{minipage}
\caption{Some steps of the prefix tree construction for the itemsets $(a,b,c)$, $(a,b,c,d)$, $(a,b,c,f)$, $(a,c,d,f)$ and $(a,c,f)$.}
\label{figure:prefix_tree}
\end{figure}

Besides the itemsets from the transactions, also all their suffixes are added to the respective prefix tree. 
This means that for the itemset $(a,b,c)$, the itemsets $(a,b,c)$, $(b,c)$ and $(c)$ are added to the trees corresponding to $a$, $b$ and $c$ respectively. 
For example, if we add the itemsets $(b, f)$ and $(b,c,d,f)$ to the previous collection, the suffixes of $(a,b,c)$, $(a,b,c,d)$ and $(a,b,c,f)$ which start with $b$, together with the new sets form the prefix in \autoref{figure:prefix_tree_b} for $b$.\\

The prefix trees are supplemented by a look-up table each. 
This table contains the items in the tree and their overall support, added together from all branches. 
Furthermore, the entries in the table are the start of linked lists through the tree which connect all nodes corresponding to the same item. 
% FIXME More details on the use of the linked list

In order to query all maximal frequent itemsets from one tree, a top-down approach is used. 
First, a candidate set is generated from the linked list. 
It contains all items with an accumulated support at least as large as the minimum support. 
The actual support has to be computed from the tree by parsing and adding up the partial support from all leaf to root paths that contain the candidate set. 
If the set is supported, it is returned as a frequent itemset (it is maximal if no super-set is found while parsing all prefix trees). 
Otherwise, all its minus-one sized subsets are processed in the same way. 
This is repeated until only frequent candidate sets remain. 

As an example, \autoref{figure:prefix_tree_ofi} shows the previous tree for $a$ and the corresponding look-up table. 
For a minimum support of 3, all items but $d$ become a member of the candidate set $(a,b,c,f)$. 
This set only is supported by a single path with support 1 and is therefore not frequent. 
Its subset $(a,b,c)$ is supported by the same path with support 3 and thus, is frequent. 
The subset $(a,c,f)$ is supported by three different paths with support 1 each. 
Therefore, it is also frequent. 
In contrast, $(a,b,f)$ is not frequent and its subsets have to be considered. 
Those are indeed frequent but they are already subsets of the other frequent itemsets and are  consequently not maximal. 

% FIXME write about pruning



\begin{figure}[t]%$[H]%[htbp]
	%	\centering
	\begin{minipage}{.4\linewidth}
		%			\centering
		\begin{figure}[H]%[htbp]
			\centering
			\begin{tikzpicture}[scale = 0.8, level/.style={sibling distance=10em/#1},
			every node/.style = {shape=circle, rounded corners,
				draw, align=center,
				top color=white, bottom color=blue!20}]
			\node {b:5}
			child { node {c:4} 
				child { node {d:2}
					child { node {f:1}}}
				child { node {f:1} }}
			child { node {f:1} };
			\end{tikzpicture}
%			\caption{Prefix tree for b from the itemsets (a,b,c), (a,b,c,d), (a,b,c,f), (a,c,d,f), (a,c,f), (b, f) and (b,c,d,f).}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.6\linewidth}
		\begin{figure}[H]%[htbp]
			\centering
			\begin{tikzpicture}[scale = 0.8, level/.style={sibling distance=10em/#1},
			every node/.style = {shape=circle, rounded corners,
				draw, align=center,
				top color=white, bottom color=blue!20}]
			\matrix at (-5,-2.5) [rectangle, bottom color=white, sharp corners, row sep=0.2cm,column sep=0.5cm]
			{
				\node[rectangle, bottom color=white, sharp corners] (B0) {b : 3}; \\ 
				\node[rectangle, bottom color=white, sharp corners] (C0) {c : 5}; \\ 
				\node[rectangle, bottom color=white, sharp corners] (D0) {d : 2}; \\ 
				\node[rectangle, bottom color=white, sharp corners] (F0) {f : 3}; \\
			};
			
			\node (a) at (0,0) {a:5}
			child { node (B1) {b:3} 
				child { node (C1) {c:3}
					child { node (D1) {d:1}}
					child { node (F1) {f:1}}}}
			child { node (C2) {c:2}
				child { node (D2) {d:1}
					child { node (F2) {f:1}}}
				child { node (F3) {f:1}} };
			\draw [red,->] (C1) edge (C2) (D1) edge (D2) (F1) edge (F2) (F2) edge (F3);
			\draw [red,->] (C0) edge (C1) (D0) edge (D1) (F0) edge (F1) (B0) edge (B1);
			\end{tikzpicture}
%			\caption{Prefix tree with look-up table and linked list through tree.}
		\end{figure}
	\end{minipage}
	\\
	\begin{minipage}{.42\linewidth}
		\caption{Prefix tree for $b$ generated from the itemsets $(a,b,c)$, $(a,b,c,d)$, $(a,b,c,f)$, $(b, f)$ and $(b,c,d,f)$.} % $(a,c,f)$, $(a,c,d,f)$, 
		\label{figure:prefix_tree_b}
	\end{minipage}
	$ $ $ $ 
	\begin{minipage}{.53\linewidth}
		\caption{Prefix tree with look-up table and linked list through tree.}
		\label{figure:prefix_tree_ofi}
		$ $ \\ $ $ 
	\end{minipage}
\end{figure}

\section{Empirical Evaluation}
\label{section:evaluation}
%Here you describe your experimental setup (data used, preprocessing or how the experiments are conducted). Then you present the results and interpret (interpretation is the most important part here) them.(see paper-structure.pdf for details)

For the empirical evaluation of the DSM-FI algorithm, it was implemented in Java. 
Its runtime and memory costs were then compared with SApriori, EStream and DCIM. 
SApriori (\cite{sun2006finding}) is an approximative version of the well-known Apriori algorithm. 
EStream (\cite{dang2008online}) also uses a prefix tree as data structure but builds up the itemsets from small to large. 
Finally, DCIM (Dynamic Confidence Interval Miner, \cite{trabold2016mining}) is based on a  probabilistic analysis of the problem. 

The data sets used are given in \autoref{table:itemsets}. 
Mostly, they were used in reduced form, meaning that all itemsets were cut off after five items. 
The numbers in brackets refer to this form. 
The first data set, \textit{retail}, is a market basket problem, the \textit{kosarak} data set is generated from click-stream data of a news website and the last one is a synthetic data set. 

\begin{table}[ht]
	\centering
	\begin{tabular}{| l | r | r | r | l |}
		\hline
		Name & $\#$ Itemsets & $\#$ Unique Items & Average Length & Source \\
		\hline
		\hline
		\textit{retail} & 88162 & 16470 (11379) & 10.3 (4.4) & \cite{brijs99using} \\
		\hline
		\textit{kosarak} & 990002 & 41486 (35883) & 8.1 (3.3) & Ferenc Bodon \\
		\hline
		\textit{T40I10D100K} & 100000 & 942 (458) & 40.6 (5.0) & IBM \\
		\hline
	\end{tabular} % FIXME add detailed citations
	\caption{Itemsets used in the evaluation. Numbers in brackets refer to reduced itemsets where the maximum itemset length is five.}
	\label{table:itemsets}
\end{table}

The procedure of the experiments was as follows. 
First, the itemsets were sorted in a preprocessing. 
Then, the prefix trees were constructed by adding all sorted itemsets. 
Finally, a query for the maximal frequent itemsets was executed.

The parameters for the comparison algorithms were chosen such that DCIM always had an $F_1$-score of 1. 
For EStream the score was also 1 in most cases and fell as low as 0.91 in a few cases. 
SApriori only achieved scores in the interval $\left[0.15, 0.43\right]$. 

All experiments were run in a virtual machine with access to 16GB memory and five processor cores (Intel Core i7-5930K @ 3.50GHz). 

\subsection{Runtime}

The runtime comparison on the reduced \textit{retail} data set in \autoref{figure:runtime1}  leads to DSM-FI runtimes two order of magnitudes larger than the competing algorithms. 
Additionally, the runtime explodes when the minimum support threshold becomes low while it is stable for the other three algorithms. 
For the threshold $s=0.1\%$, no result could be attained in reasonable time. 
Similar results are achieved on the reduced \textit{kosarak} data set in \autoref{figure:runtime2}. 
Here, the difference between DSM-FI and DCIM is less and the increase in runtime at $s=0.5\%$ starts slower. 
Again, no results for $s=0.1\%$ could be computed.
For the \textit{T40I10D100K} data set, even the results with $s=0.5\%$ could not be computed after more than 10 hours (\autoref{figure:runtime3}). 
The detailed analysis in \autoref{figure:runtime4} and \autoref{figure:runtime5} shows that the time for sorting is negligible. 
Since building the prefix trees is independent of the support threshold, the time requirements for this are stable. 
It is querying all maximal frequent itemsets that causes the extreme increase of runtime for low thresholds. 
For the faster runtime growth with \textit{retail}, the time for querying with $s=0.5\%$ completely dwarfs the time for building the trees while it only doubles for \textit{kosarak}. 


\begin{figure}[bht]%$[H]%[htbp]
	%	\centering
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_retail_l5_c.pdf}
		%		}
	\end{minipage}
	$ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_kosarak_l5_c.pdf}
		%		}
	\end{minipage}
	
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Runtimes for \textit{retail} data set.}
		\label{figure:runtime1}
	\end{minipage}
	$ $ $ $ $ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Runtimes for \textit{kosarak} data set.}
		\label{figure:runtime2}
	\end{minipage}
\end{figure}

\begin{figure}[bht]%$[H]%[htbp]
	%	\centering
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_T40I10D100K_l5_c.pdf}
		%		}
	\end{minipage}
	$ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_detailed_retail_l5_c.pdf}
		%		}
	\end{minipage}
	
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Runtimes for \textit{T40I10D100K} data set.}
		\label{figure:runtime3}
	\end{minipage}
	$ $ $ $ $ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Detailed runtimes for \textit{retail} data set.}
		\label{figure:runtime4}
	\end{minipage}
\end{figure}

\begin{figure}[bht]%$[H]%[htbp]
	%	\centering
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_detailed_kosarak_l5_c.pdf}
		%		}
	\end{minipage}
	$ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_times_parallel_c.pdf}
		%		}
	\end{minipage}
	%	\\ $ $
	
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Detailed runtimes for \textit{kosarak} data set.}
		\label{figure:runtime5}
		%		$ $ 
	\end{minipage}
	$ $ $ $ $ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Runtimes of the prefix tree generation with parallelism.}
		\label{figure:runtime6}
	\end{minipage}
\end{figure}

The reason for this effect is the enumeration of subsets that happens when candidate sets are infrequent. 
As an example, consider the itemsets  $\left( 1,30,31\right)$ and $\left( 1,2,...,29\right)$. 
Adding both to the prefix tree for $1$ and querying the frequent itemsets with minimum support 1 leads to the candidate set $\left( 1,2,...,29, 30, 31\right)$. 
Since this set is not supported, all its subsets of size 30 (not including $\left( 2,..., 31\right)$) become candidate sets and are again not supported. 
This continues for all subsets that contain at least one entry unique to each of the originally added sets. 
Therefore, almost all subsets are enumerated. 

When the minimum support threshold is low, this problem arises for many of the prefix trees and leads to the strong increase in required time. 
Since the average itemset length of \textit{T40I10D100K} is higher than of \textit{retail} and that of \textit{retail} is higher than of \textit{kosarak}, this also explains the different speeds with which their respective runtime increases.

An additional test for evaluating the effect of parallelization was executed. 
For this, observe that the algorithm often acts independently on the different prefix trees. 
In the test, the prefix tree construction was parallelized by letting multiple threads add the suffixes of an itemset to the corresponding trees in parallel. 

However, the results in \autoref{figure:runtime6} show that this does not lead to a usable improvement. 
For the reduced \textit{retail} and \textit{T40I10D100K} data set the runtime increases in most cases, especially for higher thread counts. 
With the complete \textit{retail} data set, the runtime is always lower but only using 3 threads leads to a notable improvement. 
The better results on the complete data are most likely due to the longer itemsets on average because the algorithm has to process more suffixes. 

\subsection{Memory}

For analyzing the memory consumption, the corresponding values were extracted using the Java interface \texttt{MemoryPoolMXBean}. 
On the data sets with reduced itemset length, the memory cost of DSM-FI is fairly low for the cases were the runtime does not explode. 
On \textit{retail}, it is lower than all comparison algorithms (\autoref{figure:memory1}) and on \textit{kosarak} it is similar to EStream and SApriori and lower than DCIM (\autoref{figure:memory2}). 

\begin{figure}[ht]%$[H]%[htbp]
	%	\centering
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_memory_retail_l5_c.pdf}
		
		%		}
	\end{minipage}
	%	$ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\scalebox{.9}{
		\centering
		\includegraphics[width=1.0\textwidth]{../../plots/plot_memory_kosarak_l5_c.pdf}
		%		}
	\end{minipage}
	%	\begin{minipage}{.33\linewidth}
	%		%		\scalebox{.9}{
	%%		\centering
	%		\includegraphics[width=1.0\textwidth]{../../plots/plot_memory_retail.pdf}
	%		%		}
	%	\end{minipage}
	%	\\ $ $
	
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Memory consumption for the \textit{retail} data set.}
		\label{figure:memory1}
		%		$ $ 
	\end{minipage}
	$ $ $ $ $ $ $ $ 
	\begin{minipage}{.49\linewidth}
		%		\centering
		\caption{Memory consumption for the \textit{kosarak} data set.}
		\label{figure:memory2}
	\end{minipage}
	%%	$ $ $ $ $ $ $ $ 
	%	\begin{minipage}{.33\linewidth}
	%		%		\centering
	%		\caption{Memory consumption for the complete \textit{retail} data set.}
	%		\label{figure:memory3}
	%	\end{minipage}
\end{figure}

%\begin{figure}[ht]
%	\includegraphics[width=0.5\textwidth]{../../plots/plot_memory_retail.pdf}
%	\caption{Memory consumption for the complete \textit{retail} data set.}
%	\label{figure:memory3}
%\end{figure}

\begin{wrapfigure}{r}{0.5\textwidth}
	\centering
	%\begin{center}
	\includegraphics[width=0.5\textwidth]{../../plots/plot_memory_retail_c.pdf}
	%\end{center}
	\vspace{-15pt}
	\caption{Memory consumption for the complete \textit{retail} data set.}
	\label{figure:memory3}
	\vspace{-10pt}
\end{wrapfigure}

However, for the complete \textit{retail} set, the memory cost of DSM-FI is significantly higher than for the other algorithms (\autoref{figure:memory3}). 
For complete \textit{kosarak} and complete \textit{T40I10D100K} the algorithms runs out of memory. 
Profiling the memory cost showed that it is dominated by the number of nodes required for the prefix trees.


\section{Conclusion}
\label{section:conclusion}

The results of our evaluation imply that the algorithm is not practical in general. 
The explosion of the runtime for low support thresholds makes the algorithm impossible to use in such cases. 
Especially, the goal of being able to query efficiently at any given point, is not fulfilled here. 
Furthermore, the memory cost is too high for large data sets even when using feasible thresholds. 
This defeats the purpose of the summarizing data structure. 
%It must be noted however that the memory result in the original paper are significantly lower than our results. 
%Discuss your approach and to what extend it solves the problem stated in the introduction. (see paper-structure.pdf for details)

\bibliographystyle{plainnat}
\bibliography{refs.bib}
\end{document}
